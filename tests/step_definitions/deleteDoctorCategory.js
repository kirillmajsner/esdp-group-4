const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Список категорий врачей', () => {
    I.amOnPage('admin/doctor_category');
});

When('я нажимаю на кнопку {string}', (fieldName) => {
    I.click({xpath:`//button[@id='${fieldName}']`});
});

Then('я увижу текст {string}', text => {
    I.waitForText(text);
});