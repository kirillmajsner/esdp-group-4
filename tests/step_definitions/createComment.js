const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Новости', () => {
    I.amOnPage('news');
});


When('я нажимаю ссылку {string}', (fieldName) => {
    I.click({xpath: `//a[@id='${fieldName}']`});
});

When('я ввожу комментарий {string} в поле {string}', (text, fieldName) => {
    I.fillField({xpath:`//input[@id='${fieldName}']`}, text)
});

When('я нажимаю кнопку {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

