const {I} = inject();
// Add in your custom step files

When('я нахожусь на странице Список клиник', () => {
    I.amOnPage('admin/clinics');
});

When('я нажимаю на кнопку {string}', (fieldName) => {
    I.click({xpath:`//button[@id='${fieldName}']`});
});

Then('статус записи изменяется на {string}', text => {
    I.waitForText(text);
});