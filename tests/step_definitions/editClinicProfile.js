const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Общие данные клиники', () => {
    I.amOnPage('cabinet/clinic_profile');
});

When('я нажимаю кнопку {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

When('я нахожусь на странице Анкета', () => {
    I.amOnPage('cabinet/profile');
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath: `//textarea[@id='${fieldName}']`}, text)
});

Then('я вижу текст ответа от сервера {string}', text => {
    I.waitForText(text);
});



