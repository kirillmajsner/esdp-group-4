const {I} = inject();
// Add in your custom step files


When('я нахожусь на странице Анкета', () => {
    I.amOnPage('cabinet/profile');
});

When('я ввожу {string} в {string}', (text, fieldName) => {
    I.fillField({xpath: `//input[@id='${fieldName}']`}, text)
});

When('я нажимаю на див {string}', () => {
    I.click({xpath:`//div[@class='react-datepicker-wrapper']`});
    I.click("//div[contains(@class, 'react-datepicker__day') and text() = '3']");
});

When('я выбираю {string} в {string}', (text, fieldName) => {
    I.selectOption({xpath:`//select[@id='${fieldName}']`}, text);
});

When('я нажимаю кнопку для отправки {string}', (fieldName) => {
    I.click({xpath: `//button[@id='${fieldName}']`});
});

Then('я вижу текст ответа от сервера {string}', text => {
    I.waitForText(text);
});



