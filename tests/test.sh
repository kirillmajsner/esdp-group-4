#!/bin/bash

CURRENT_DIR=`dirname $0`
cd $CURRENT_DIR
CURRENT_DIR=`pwd`

cd $CURRENT_DIR/../api

NODE_ENV=test yarn run seed

pm2 start "yarn run start:test" --name="esdp-api-test"

cd $CURRENT_DIR/../client

pm2 start "yarn run start:test" --name="esdp-client-test"

echo $CURRENT_DIR
cd $CURRENT_DIR

while ! nc -z localhost 3010; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

yarn start || true

pm2 kill
