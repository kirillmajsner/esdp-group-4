
## Проект "Название"

-база данных всех врачей и клиник Кыргызстана, с полным их описанием;  
-онлайн запись к врачу через приложение;  
-онлайн мед.карта с возможность заносить в неё всю цифровую информацию о пациенте (фото, видео, анализы, диагностические снимки);   
-помощь врачу или пациенту в случае конфликта, осложнений или врачебной ошибки (помощь будут оказывать комиссия из врачей, юристов, адвокатов);  
-онлайн консультация;

## Используемые технологии 

### `Backend`

База данных [MongoDB](https://www.mongodb.com/).  

Платформа [Node.js](https://nodejs.org/en/about/).  

Библиотека для базы данных [Mongoose](https://mongoosejs.com/), версия 5.5.11.  
Web фреймворк для Node.js [express](https://github.com/expressjs), версия 4.17.1.  
Шифрование данных с помощью [bcrypt](https://www.npmjs.com/package/bcrypt), версия 3.0.6.  
Хранение multipart/form-data с помощью [multer](https://www.npmjs.com/package/multer), версия 1.4.1.  
Генератор случайных строк  [nanoid](https://www.npmjs.com/package/nanoid), версия 2.0.3.  

Команда запуска `npm run start`

Команда запуска fixtures.js на тестовом сервере `NODE_ENV=test yarn run seed`

### `Frontend`

Приложение создано на [Create React App](https://github.com/facebook/create-react-app).  
  
  
Сетевые запросы [axios](https://www.npmjs.com/package/axios), версия 0.18.0.  
Bootstrap компоненты [reactstrap](https://reactstrap.github.io/), версия 8.0.0.  
State контейнер:   
[Redux](https://redux.js.org/introduction/getting-started), версия 4.0.1.  
[React-redux](https://react-redux.js.org/), версия 7.0.3.  
[Redux-thunk](https://github.com/reduxjs/redux-thunk), версия 2.3.0.  


Команда запуска  `npm start`


## Разработчики

1. Алина Бибикова   
2. Дастан Максатбеков   
3. Кирилл Майснер   
4. Мурат Кубибаев   
5. Дженалиева Айсалкын   


