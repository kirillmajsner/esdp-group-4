const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MedicalCardSchema = new Schema({
    patientId: {
        type: Schema.Types.ObjectId,
        ref: 'PatientProfile',
        required: true
    },
    doctorId: {
        type: Schema.Types.ObjectId,
        ref: 'DoctorProfile',
        required: true
    },
    pastIllnesses: {
        type: String
    },
    presentDisease: {
        type: String
    },
    diagnosis: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    }
});

const MedicalCard = mongoose.model('MedicalCard', MedicalCardSchema);

module.exports = MedicalCard;