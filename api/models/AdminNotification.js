const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AdminNotificationSchema = new Schema({
    text: {
        type: String,
        required: true
    },
    datetime: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ["new", "read"],
        default: "new"
    }
});

const AdminNotification = mongoose.model('AdminNotification', AdminNotificationSchema);

module.exports = AdminNotification;