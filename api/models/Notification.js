const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const NotificationSchema = new Schema({
    notification: {
        type: String,
        required: true
    },
    datetime: {
        type: String,
        required: true
    },
    record: {
        type: Schema.Types.ObjectId,
        ref: 'Record',
        required: true
    },
    patient: {
        type: Schema.Types.ObjectId,
        ref: 'PatientProfile'
    },
    doctor: {
        type: Schema.Types.ObjectId,
        ref: 'DoctorProfile'
    },
    patientStatus: {
        type: String,
        enum: ["new", "read"],
        default: "new"
    },
    doctorStatus: {
        type: String,
        enum: ["new", "read"],
        default: "new" 
    }
});

const Notification = mongoose.model('Notification', NotificationSchema);

module.exports = Notification;