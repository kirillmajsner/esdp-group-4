const path = require('path');

const rootPath = __dirname;

const dbUrl = process.env.NODE_ENV === 'test' ? 'mongodb://localhost/esdp_test': 'mongodb://localhost/esdp';
const wsURL = process.env.NODE_ENV === 'start' ? 'http://167.71.77.224:8000': 'http://localhost:8000';

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl,
    wsURL,
    mongoOptions: {
        useNewUrlParser: true,
        useCreateIndex: true
    },
    mailUser: 'projetp901@gmail.com',
    mailPass: 'QwErTy123'
};

