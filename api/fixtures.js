const mongoose = require('mongoose');
const nanoid = require('nanoid');
const config = require('./config');

const ClinicProfile = require('./models/ClinicProfile');
const User = require('./models/User');
const Profile = require('./models/Profile');
const DoctorCategory = require('./models/DoctorCategory');
const DoctorProfile = require('./models/DoctorProfile');
const PatientProfile = require('./models/PatientProfile');
const Record = require('./models/Record');
const ShopProfile = require('./models/ShopProfile');
const News = require('./models/News');
const Comment = require('./models/Comment');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const profiles = await Profile.create(
        {title: 'Доктор', codeName: 'doctor', description: 'Описание профиля доктора'},
        {title: 'Пациент', codeName: 'patient', description: 'Описание профиля пациента'},
        {title: 'Магазин', codeName: 'shop', description: 'Описание профиля магазина'},
        {title: 'Клиника', codeName: 'clinic', description: 'Описание профиля клиники'},
    );

    const doctorCategories = await DoctorCategory.create(
        {doctorCategory: "Стоматолог"},
        {doctorCategory: "Хирург"},
        {doctorCategory: "Педиатр"},
        {doctorCategory: "Терапевт"},
        {doctorCategory: "Кардиолог"},
        {doctorCategory: "Травмотолог"},
        {doctorCategory: "Лор"},
        {doctorCategory: "Окулист"},
    );

    const users = await User.create(
        {
            password: '123',
            role: 'admin',
            token: nanoid(),
            profile: profiles[1],
            email: "admin@mail.ru",
            activate: true,
            activateToken: false,
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc1@mail.ru",
            image: 'doc4.jpg',
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc2@mail.ru",
            image: 'doc2.jpg',
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc3@mail.ru",
            image: 'doc3.jpg',
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc4@mail.ru",
            image: 'doc4.jpg',
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc5@mail.ru",
            image: 'doc5.jpg',
        },
        {
            password: '123',
            activate: true,
            activateToken: false,
            token: nanoid(),
            profile: profiles[0],
            email: "doc6@mail.ru",
            image: 'doc6.jpg',
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[1],
            email: "patient@example.com"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[1],
            email: "patient@example.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[0],
            email: "doctor@example.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[3],
            email: "clinic@example.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[3],
            email: "clinic@example-2.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[2],
            email: "shop@example.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[2],
            email: "shop@example-2.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[3],
            email: "clinic1212@example.kg",
            image: 'onclinic.png'
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[3],
            email: "clinic121212@example.kg"
        },
        {
            password: '123',
            token: nanoid(),
            activate: true,
            activateToken: false,
            profile: profiles[3],
            email: "clinic12@example.kg",
            image: 'neomed.jpeg'
        }
    );


    const shops = await ShopProfile.create(
        {
            title: "Ананайка кидс",
            description: "Семейный магазин",
            address: "г.Бишкек, ул. Ибраимова, 84",
            phone: "+996 555–43–97–57",
            user: users[12],
            approved: "approved"
        }
    );

    const clinics = await ClinicProfile.create(
        {
            title: "Кроха",
            description: "Семейная клиника",
            address: "г.Бишкек, ул. Ибраимова, 84",
            phone: "+996 555–43–97–57",
            user: users[10],
            approved: "approved"
        },
        {
            title: "Неомед",
            description: "Клиника",
            address: "г. Бишкек, ул. Орозбекова 46",
            phone: "+996 312-90-60-90",
            user: users[16],
            approved: "approved"
        },
        {
            title: "Малыш",
            description: "Семейная клиника",
            address: "г.Бишкек, ул. Линейная, 67",
            phone: "+996 312-30–19–19",
            user: users[15],
            approved: "pending"
        },
        {
            title: "On Clinic",
            description: "Клиника",
            address: "г. Бишкек, ул. Веселая 46",
            phone: "+996 312-90-90-95",
            user: users[14],
            approved: "approved"
        }
    );

    const doctors = await DoctorProfile.create(
        {
            name: "Иван",
            surname: "Иванов",
            thirdname: "Иванович",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "male",
            price: 500,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[1],
            workAddress: "Бишкек, Горького 3",
            approved: "pending"
        },
        {
            name: "Петр",
            surname: "Пертров",
            thirdname: "Петрович",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "male",
            price: 600,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[2],
            workAddress: "Бишкек, Горького 3",
            approved: "approved"
        },
        {
            name: "Ася",
            surname: "Василиьева",
            thirdname: "Васильевна",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "female",
            price: 700,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[3],
            workAddress: "Бишкек, Горького 3",
            approved: "approved"
        },
        {
            name: "Дмитрий",
            surname: "Дмитриев",
            thirdname: "Дмитриевич",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "male",
            price: 500,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[4],
            workAddress: "Бишкек, Горького 3",
            approved: "pending"
        },
        {
            name: "Александр",
            surname: "Козлов",
            thirdname: "Алексеевич",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "male",
            price: 500,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[5],
            workAddress: "Бишкек, Горького 3",
            approved: "pending"
        },
        {
            name: "Никита",
            surname: "Петров",
            thirdname: "Иванович",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            license: "КР1212121",
            dateOfBirth: "12.12.12",
            yearsOfWork: 10,
            diploma: "КР1212121",
            clinic: clinics[1],
            schedule: "9.00 до 18.00",
            gender: "male",
            price: 500,
            doctorCategory: doctorCategories[0],
            speciality: "Стоматолог",
            user: users[6],
            workAddress: "Бишкек, Горького 3",
            approved: "approved"
        },
    );

    const patients = await PatientProfile.create(
        {
            name: "Василий",
            surname: "Петров",
            thirdname: "Искакович",
            dateOfBirth: "12.12.12",
            gender: "male",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            workAddress: "ТЭЦ КР",
            residence: "ул. Ауэзова 50/2",
            user: users[7]
        },
        {
            name: "Василий",
            surname: "Петров",
            thirdname: "Искакович",
            dateOfBirth: "12.12.12",
            gender: "male",
            passportId: "ID",
            passportNumber: 12132121,
            phone: "+996 555 555 555",
            workAddress: "ТЭЦ КР",
            residence: "ул. Ауэзова 50/2",
            user: users[0]
        }
    );


    const records = await Record.create(
        {
            date: "12.07.2019",
            time: "15:00",
            patientConfirm: true,
            doctorId: doctors[1],
            patientId: patients[0],
            complaint: "Зуб болит"
        },
        {
            date: "28.06.2019",
            time: "16:30",
            patientConfirm: true,
            doctorConfirm: true,
            doctorId: doctors[2],
            patientId: patients[0],
            status: "approved",
            complaint: "Зуб болит"
        },
        {
            date: "11.07.2019",
            time: "09:30",
            patientConfirm: false,
            doctorConfirm: true,
            doctorId: doctors[2],
            patientId: patients[0],
            status: "rescheduled",
            complaint: "Зуб болит"
        }
    );

    const news = await News.create(
        {
            title: "Новость",
            description: "Описание Новости",
            image: "putin.jpeg",
            user: users[10],
        },
        {
            title: "Новость",
            description: "Описание Новости",
            image: "putin.jpeg",
            user: users[10],
        },
        {
            title: "Новость",
            description: "Описание Новости",
            image: "putin.jpeg",
            user: users[10],
        }
    );

    const comments = await Comment.create(
        {
            description: "Описание Новости",
            news: news[0],
            user: users[10],
        },
        {
            description: "Описание Новости",
            news: news[1],
            user: users[10],
        },
        {
            description: "Описание Новости",
            news: news[2],
            user: users[10],
        }
    );


    await connection.close();
};


run().catch(error => {
    console.log('Something went wrong', error);
});
