const express = require('express');
const auth = require('../middleware/auth');
const DoctorProfile = require('../models/DoctorProfile');
const axios = require('axios');
const config = require('../config');

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        let criteria = {approved: "approved"};
        let select = {};

        if (req.query.doctorCategory) {
            criteria = {
                doctorCategory: req.query.doctorCategory,
                approved: "approved"
            };
            select = {
                name: "name",
                surname: "surname",
                speciality: "speciality",
                yearsOfWork: "yearsOfWork",
                phone: "phone",
                price: "price",
                rating: 'rating'
            }
        }
        if (req.query.clinic) {
            criteria = {
                clinic: req.query.clinic,
                approved: "approved"
            };
            select = {
                name: "name",
                surname: "surname",
                speciality: "speciality",
                yearsOfWork: "yearsOfWork",
                phone: "phone",
                price: "price",
                rating: 'rating'
            }
        }
        if (req.query.user) {
            criteria = {
                user: req.query.user
            };
        }
        const doctor = await DoctorProfile.find(criteria).populate({
            path: "user clinic doctorCategory",
            select: {image: 'image', title: 'title', address: "address", doctorCategory: "doctorCategory"}
        }).select(select);
        res.send(doctor);
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/:id', (req, res) => {
    DoctorProfile.findById(req.params.id).populate({
        path: "user clinic",
        select: {image: 'image', title: 'title', address: "address"}
    }).select({
        name: "name",
        surname: "surname",
        speciality: "speciality",
        yearsOfWork: "yearsOfWork",
        phone: "phone",
        price: "price",
        rating: "rating"
    })
        .then(doctor => {
            if (doctor) res.send(doctor);
            else res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});


router.post('/', auth, async (req, res) => {
    const data = req.body;
    data.user = req.user._id;

    try {
        const doctorProfile = await new DoctorProfile(data);
        await doctorProfile.save();

        const notification = {
            text: "Новый аккаунт Доктора отправлен на утверждение",
        };
        await axios.post(config.wsURL + '/admin/notifications', notification);
        return res.send({message: 'Анкета сохранена'});
    } catch (e) {
        return res.status(400).send(e);
    }
});

router.put('/', auth, async (req, res) => {
    let doctor = await DoctorProfile.findOne({user: req.user._id});
    if (doctor.user.equals(req.user._id)) {

        doctor.approved = "pending";
        doctor.name = req.body.name;
        doctor.surname = req.body.surname;
        doctor.thirdname = req.body.thirdname;
        doctor.passportId = req.body.passportId;
        doctor.passportNumber = req.body.passportNumber;
        doctor.phone = req.body.phone;
        doctor.license = req.body.license;
        doctor.dateOfBirth = req.body.dateOfBirth;
        doctor.yearsOfWork = req.body.yearsOfWork;
        doctor.diploma = req.body.diploma;
        doctor.clinic = req.body.clinic;
        doctor.schedule = req.body.schedule;
        doctor.gender = req.body.gender;
        doctor.price = req.body.price;
        doctor.doctorCategory = req.body.doctorCategory;
        doctor.speciality = req.body.speciality;
        doctor.workAddress = req.body.workAddress;

        await doctor.save()
            .then(result => res.send({result, message: "Анкета изменена"}))
            .catch(error => res.status(400).send(error))
    } else {
        res.status(403).send(error);
    }

});

router.put('/:id', auth, async (req, res) => {
    let doctor = await DoctorProfile.findById(req.params.id);
    let index = doctor.rating.findIndex(obj => {
        return obj.medicalCard === req.body.medicalCard});
    if (index === -1) {
        doctor.rating.push({
            patient: req.body.patient,
            rating: req.body.rating,
            medicalCard: req.body.medicalCard,
            review: req.body.review,
            datetime: new Date().toISOString(),
            anonymous: req.body.anonymous,
            name: req.body.name,
            surname: req.body.surname
        });
        await doctor.save()
            .then(result => res.send({result, message: "Рейтинг выставлен"}))
            .catch(error => res.status(400).send("Оценить врача могут только лечащиеся пациенты"));
    } else {
        res.status(400).send("Оценить врача можно всего раз за лечение")
    }


});
module.exports = router;