const express = require('express');

const Profile = require('../models/Profile');
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const profiles = await Profile.find();

        return res.send(profiles);
    } catch {
        return res.sendStatus(500);
    }
});

module.exports = router;