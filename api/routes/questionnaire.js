const express = require('express');
const Questionnaire = require('../models/Questionnaire')

const auth = require('../middleware/auth');

const router = express.Router();

router.post('/', auth, async (req, res) => {
    try {
        const data = req.body;
        const questionnaire = await new Questionnaire(
            {
                patientId: req.query.patient,
                doctorId: req.user._id,
                questionnaire: data.questionnaire
            });
        questionnaire.save()
        return res.send({message: 'Анкета успешно сохранена'})
    } catch (e) {
        return res.status(400).send(e);
    }
})

router.get('/:id', auth, async (req, res) => {
    try {
        const response = await Questionnaire.find({patientId: req.params.id})
        return res.send(response)
    } catch (e) {
        res.sendStatus(400).send(e)
    }
});

router.put('/edit/:id', auth, async (req, res) => {
    try {
        let quest = await Questionnaire.findById(req.params.id);

        if (!quest) {
            return res.sendStatus(404);
        }
        quest.questionnaire = req.body.questionnaire
        quest.save()
        return res.send({message: 'Анкета успешно обновлена'})

    } catch (e) {
        return res.sendStatus(400).send(e);
    }
});

module.exports = router;
