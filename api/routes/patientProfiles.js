const express = require('express');
const auth = require('../middleware/auth');
const Patients = require('../models/PatientProfile');

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        if (req.query.name && !req.query.surname) {
            let patient = await Patients.findOne({name: req.query.name}).select("name surname thirdname dateOfBirth");
            return res.send(patient);
        } else if (req.query.surname && !req.query.name) {
            let patient = await Patients.findOne({surname: req.query.surname}).select("name surname thirdname dateOfBirth");
            return res.send(patient);
        } else if (req.query.surname && req.query.name) {
            let patient = await Patients.findOne({surname: req.query.surname, name: req.query.name}).select("name surname thirdname dateOfBirth");
            return res.send(patient);
        } else {
            let patient = await Patients.findOne({user: req.user._id});
            if (patient.user.equals(req.user._id)) {
                return res.send(patient);
            }
        }
    } catch (e) {
        res.status(500).send(e);
    }
});


router.post('/', auth, async (req, res) => {
    const patientData = req.body;
    patientData.user = req.user._id;
    const patient = await new Patients(patientData);
    patient.save()
        .then(result => res.send({result, message: "Ваша анкета сохранена"}))
        .catch(error => res.status(400).send(error))
});


router.put('/', auth, async (req, res) => {
    let patient = await Patients.findOne({user: req.user._id});
    if (patient.user.equals(req.user._id)) {
        patient.name = req.body.name;
        patient.surname = req.body.surname;
        patient.thirdname = req.body.thirdname;
        patient.dateOfBirth = req.body.dateOfBirth;
        patient.gender = req.body.gender;
        patient.passportId = req.body.passportId;
        patient.passportNumber = req.body.passportNumber;
        patient.phone = req.body.phone;
        patient.workAddress = req.body.workAddress;
        patient.residence = req.body.residence;

        patient.save()
            .then(result => res.send({result, message: "Анкета изменена"}))
            .catch(error => res.status(400).send(error))
    } else {
        res.sendStatus(403);
    }

});
module.exports = router;