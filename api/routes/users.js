const express = require('express');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const secret = 'harryPotter';


const User = require('../models/User');
const Profile = require('../models/Profile');
const config = require('../config');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const auth = require('../middleware/auth');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    auth: {
        user: config.mailUser,
        pass: config.mailPass
    }
});

router.post('/', async (req, res) => {

    const data = req.body;
    const user = await new User(
        {
            email: data.email,
            password: data.password,
            profile: data.profile,
            activateToken: jwt.sign({email: data.email}, secret, {expiresIn: '24h'})
        }
    );


    user.generateToken();

    try {
        if (data.email === null || data.email === '') {
            console.log('Заполните указанные поля');
        } else {
            const email = {
                from: '"Localhost:8000',
                to: data.email,
                subject: 'Активация аккаунта',
                html: 'Здравствуйте <strong>' + data.email + '</strong>.<br>Cпасибо за регистрацию на сайте http://clinic.serveirc.com' +
                    '<br>  Пожалуйста, перейдите по ссылке для завершения регистрации: <a href="http://clinic.serveirc.com/verify/'
                    + user.activateToken + '">http://clinic.serveirc.com/verify/</a>'
            };

            transporter.sendMail(email, (err, res) => {
                if (err) {
                    console.log(err);
                } else {
                    console.log('Done')
                }
            });
        }

        await user.save();
        const profile = await Profile.findById(user.profile);
        user.profile = profile;

        return res.send({message: 'Регистрация прошла успешно', user});
    } catch (error) {
        return res.status(400).send(error.errors)
    }
});

router.put('/verify/:token', async (req, res) => {
    const user = await User.findOne({activateToken: req.params.token});

    if (!user) {
        return res.sendStatus(404);
    }

    const token = req.params.token;


    jwt.verify(token, secret, (err, decoded) => {
        if (err) {
            return res.send({message: 'Ссылка не активна'});
        } else if (!user) {
            return res.send({message: 'Ссылка не активна'})
        } else {
            user.activateToken = false;
            user.activate = true;

            user.save(err => {
                if (err) {
                    console.log(err);
                } else {

                    const email = {
                        from: '"Localhost:8000" @localhost.com',
                        to: user.email,
                        subject: 'Localhost Активация Аккаунта',
                        html: 'Здравтсвуйте <strong>' + user.email + '</strong>, <br>Ваш аккаунт успешно активирован'
                    };

                    transporter.sendMail(email, (err, response) => {
                        if (err) {
                            console.log(err);
                        } else {
                            console.log('Аккаунт активирован!')
                        }
                    });
                    return res.send({message: 'Аккаунт активирован!'});
                }
            });
        }
    })
});

router.put('/', auth, upload.single('image'), async (req, res) => {
    let user = await User.findById(req.user._id);
    if (req.file) {
        user.image = req.file.filename;
        await user.save()
            .then(() => res.send({user, message: "Фотография добавлена"}))
            .catch(error => res.status(404).send(error))
    } else {
        const isMatch = await user.checkPassword(req.body.oldPassword);

        if (!isMatch) {
            return res.status(400).send({error: 'Неверный пароль!'});
        }
        user.password = req.body.password;
        await user.save()
            .then(() => res.send({user, message: "Пароль успешно измненен"}))
            .catch(error => res.status(404).send(error))

    }


});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({email: req.body.email}).populate('profile', 'codeName');
    if (!user) {
        return res.status(400).send({error: 'Неверный логин или пароль!'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Неверный логин или пароль!'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Вы вошли успешно', user});
});

router.delete('/sessions', async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Вы вышли успешно!'};

    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    return res.send(success);
});


module.exports = router;