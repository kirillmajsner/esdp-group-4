import React from 'react';
import {Route, Switch, Redirect} from "react-router-dom";


import AdminLogin from "./containers/AdminContainers/AdminLogin";
import ClinicProfile from "./containers/AdminContainers/ClinicProfile";
import AdminClinicsList from "./containers/AdminContainers/AdminClinicsList";
import AdminDoctorsList from "./containers/AdminContainers/AdminDoctorsList";
import DoctorProfile from "./containers/AdminContainers/DoctorProfile";
import DoctorCategoryAddForm from "./containers/AdminContainers/DoctorCategoryAddForm";
import DoctorCategoriesList from "./containers/AdminContainers/DoctorCategoriesList";
import DoctorCategoryEditForm from "./containers/AdminContainers/DoctorCategoryEditForm";
import AdminMainPage from "./containers/AdminContainers/AdminMainPage";
import AdminNotificationsList from "./containers/AdminContainers/AdminNotificationsList";


const ProtectedRoute = ({isAllowed, ...props}) => (
    isAllowed ? <Route {...props} /> : <Redirect to="admin/login"/>
);

const Routes = ({user}) => {
    return (
        <Switch>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin" exact component={AdminMainPage}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/clinics" exact component={AdminClinicsList}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/doctor_category/new" exact component={DoctorCategoryAddForm}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/doctor_category" exact component={DoctorCategoriesList}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/doctor_category/:id/edit" component={DoctorCategoryEditForm}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/doctors" exact component={AdminDoctorsList}/>
            <Route path="/admin/login" exact component={AdminLogin}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/clinic/:id" exact component={ClinicProfile}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/doctor/:id" exact component={DoctorProfile}/>
            <ProtectedRoute isAllowed={user && user.role === 'admin'}
                            path="/admin/notifications" exact component={AdminNotificationsList}/>

        </Switch>
    )
};

export default Routes;


