import React from 'react'

import {shallow} from 'enzyme';
import Footer from '../../Components/Footer/Footer'

describe('Footer', () => {
    it('should render correctly', () => {
        const component = shallow(<Footer />)
        const actual = component.html().includes('div')
        expect(actual).toBeTruthy()
    });

})