import React from 'react'

import {shallow} from 'enzyme';
import DoctorPage from "../../Components/DoctorPage/DoctorPage";

describe('DoctorPage', () => {
    it('should render without crash', () => {
        const wrapper = shallow(<DoctorPage/>);
        expect(wrapper).toHaveLength(1);
    });
})
