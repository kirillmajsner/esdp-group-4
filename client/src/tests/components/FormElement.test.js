import React from 'react'

import {shallow} from 'enzyme';
import {FormElement} from '../../Components/UI/Form/FormElement';

describe('FormElement', () => {

    it('should render correctly', () => {
        const component = shallow(
            <FormElement propertyName={'someId'}/>
        )
        const actual = component.html().includes('div')
        expect(actual).toBeTruthy()
    })

    it('should render with title of prop', () => {
        const component = shallow(<FormElement propertyName={''} title={'title'} />)
        const actual = component.html().includes('div')
        expect(actual).toBeTruthy()
    });

    it('should render with error of prop', () => {
        const component = shallow(<FormElement propertyName={''} error={'some error message'} />)
        const actual = component.html().includes('div')
        expect(actual).toBeTruthy()
    });

    it('should render with value of prop', () => {
        const component = shallow(<FormElement propertyName={''} value={''} onChange={() => null} />)
        const actual = component.html().includes('div')
        expect(actual).toBeTruthy()
    });

})