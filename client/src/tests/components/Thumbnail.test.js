import React from 'react'

import {shallow} from 'enzyme';
import Thumbnail from "../../Components/Thumbnail/Thumbnail";

describe('Thumbnail', () => {
    it('should render with image as props', () => {
        const image = 'doc1.jpg'
        const wrapper = shallow(<Thumbnail props={image} />);
        expect(wrapper).toBeTruthy();
    });
})
