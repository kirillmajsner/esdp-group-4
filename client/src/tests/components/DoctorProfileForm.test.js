import React from 'react'

import {shallow} from 'enzyme';
import DoctorProfileForm from "../../Components/DoctorProfileForm/DoctorProfileForm";

describe('DoctorProfileForm', () => {
    it('should render with props', () => {
        const categories = ['One', 'Two']
        const clinics = ['clinic-1', 'clinic-2']
        const wrapper = shallow(
            <DoctorProfileForm
            doctorCategories={categories}
            clinics={clinics}
            onSubmit={() => null}
            />);
        expect(wrapper).toHaveLength(1);
    });
})
