import React, {Component} from 'react';
import {Button, Card, CardTitle, Col, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";

import {fetchMedicalCards, fetchPatientId} from "../../store/actions/medicalCards";
import {connect} from "react-redux";
import Moment from "react-moment";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";

class PatientMedicalCards extends Component {

    componentDidMount() {
        if(this.props.patient){
            this.props.fetchMedicalCards(this.props.patient._id);
        }

    }

    render() {
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    {this.props.cards.length > 0 ?
                        this.props.cards.map(item => (
                        <Col xs={12} md={4} key={item._id}>
                            <Card body className={'medicalCards-card'}>
                                <CardTitle>
                                    <span style={{paddingRight: '5px'}}>Медицинская карта от:</span>
                                    {item.date
                                        ? <Moment format="D MMM YYYY HH:mm"
                                                  withTitle>{item.date}
                                        </Moment>
                                        : 'дата не определена'}
                                </CardTitle>
                                <Button color={'info'} size={'sm'}>
                                    <NavLink tag={RouterNavLink}
                                             className={'medicalCard_navLink'}
                                             to={`/cabinet/cards_list/${item._id}`}
                                             exact
                                    >
                                        Смотреть
                                    </NavLink>
                                </Button>
                            </Card>
                        </Col>
                    ))
                        : <p>У вас нет записей в истории лечения</p>
                    }
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    patient: state.users.patientProfile,
    cards: state.medicalCards.medicalCards,
});

const mapDispatchToProps = (dispatch) => ({
    fetchPatientId: data => dispatch(fetchPatientId(data)),
    fetchMedicalCards: id => dispatch(fetchMedicalCards(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientMedicalCards);