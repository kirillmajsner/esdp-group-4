import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import ClinicProfileForm from "../../Components/ClinicProfileForm/ClinicProfileForm";
import {editProfile} from "../../store/actions/profilesActions";


class ClinicProfileEdit extends Component {

    editProfile = data => {
        this.props.editProfile(data);
    };

    render() {
        return (
            <Fragment>
                <ClinicProfileForm onSubmit={this.props.editProfile} error={this.props.error}
                                   data={this.props.clinicProfile}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error,
    clinicProfile: state.users.clinicProfile
});

const mapDispatchToProps = dispatch => ({
    editProfile: (path, profileData) => dispatch(editProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ClinicProfileEdit);
