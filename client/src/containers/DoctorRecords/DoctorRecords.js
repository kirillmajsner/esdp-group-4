import React, {Component, Fragment} from 'react';
import {
    Badge,
    Button,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader, NavLink,
    Row,
    Table,
    UncontrolledTooltip
} from "reactstrap";
import {connect} from "react-redux";
import Calendar from "react-calendar";
import {faCheckSquare, faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import 'moment/locale/ru';

import {changeRecord, fetchDoctorRecords, openModal} from "../../store/actions/recordsActions";
import "./DoctorRecords.css";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";
import RescheduleBooking from "../../Components/RescheduleBooking/RescheduleBooking";
import {Link} from "react-router-dom";
import {NavLink as RouterNavLink} from "react-router-dom";


class DoctorRecords extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: "",
            date: new Date(),
            data: "",
            rescheduleId: ""
        };

        this.toggle = this.toggle.bind(this);
    }


    componentDidMount() {
        if (this.props.doctorProfile) {
            const date = this.state.date.toLocaleDateString('ru-Ru');
            this.props.fetchDoctorRecords(this.props.doctorProfile._id, date);
        }

    }

    toggleRescheduled = (rescheduleId, id) => {
        this.setState(prevState => ({
            rescheduleId: rescheduleId,
            id,
            data: {
                patientConfirm: false,
                status: "rescheduled",
                doctorConfirm: true
            }
        }));
        this.props.toggleModal();
    };


    toggle = (id) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            id: id
        }));
    };


    onChange = date => {
        this.props.fetchDoctorRecords(this.props.doctorProfile._id, date.toLocaleDateString('ru-Ru'));
        this.setState({date});
    };

    acceptTime = id => {
        const data = {
            doctorConfirm: true,
            status: "approved"
        };

        const date = this.state.date.toLocaleDateString('ru-Ru');

        this.props.changeRecord(id, data, date);
        this.props.websocket.send(JSON.stringify({type: "CHANGE_RECORD", profile: "doctor", id: id}))
    };

    declineRecord = id => {
        const data = {
            doctorConfirm: false,
            status: "declined",
        };

        const date = this.state.date.toLocaleDateString('ru-Ru');

        this.props.changeRecord(id, data, date);
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
        this.props.websocket.send(JSON.stringify({type: "CHANGE_RECORD", profile: "doctor", id: id}))
    };

    render() {
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Fragment>
                        <Calendar
                            onChange={this.onChange}
                            value={this.state.date}
                        />
                        {this.props.doctorRecords.length > 0 ?
                            <div className="table-container">
                                <Table className="my-4" bordered>
                                    <thead>
                                    <tr>
                                        <th scope="row">№</th>
                                        <th scope="row">Дата</th>
                                        <th scope="row">Время</th>
                                        <th scope="row">Статус</th>
                                        <th scope="row">Пациент</th>
                                        <th scope="row">Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.props.doctorRecords.map((record, index) => {
                                        let status;
                                        let button;
                                        if (record.status === "new" && record.patientConfirm) {
                                            status = <Badge color="primary">Отправлена</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltipss' + index} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/><UncontrolledTooltip placement="right"
                                                                                         target={"Tooltipss" + index}>
                                                    Отклонить
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip4' + index} className="mx-2" type="button"
                                                        color="success"
                                                        onClick={() => this.acceptTime(record._id)}><FontAwesomeIcon
                                                    icon={faCheckSquare}/><UncontrolledTooltip placement="right"
                                                                                               target={"Tooltip4" + index}>
                                                    Принять
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip6' + index} className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                        target={"Tooltip6" + index}>
                                                    Перенести
                                                </UncontrolledTooltip></Button>
                                            </Fragment>;
                                        }
                                        if (record.status === "new" && record.doctorConfirm) {
                                            status = <Badge color="primary">Отправлена Вами</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltipss' + index} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/><UncontrolledTooltip placement="right"
                                                                                         target={"Tooltipss" + index}>
                                                    Отклонить
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip6' + index} className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                        target={"Tooltip6" + index}>
                                                    Перенести
                                                </UncontrolledTooltip></Button>
                                            </Fragment>;
                                        }
                                        if (record.status === "rescheduled") {
                                            status = <Badge color="warning">Перенесена</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltips' + index} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/>
                                                    <UncontrolledTooltip placement="right"
                                                                         target={"Tooltips" + index}>
                                                        Отклонить
                                                    </UncontrolledTooltip>
                                                </Button>
                                                <Button id={'Tooltip3' + index} className="mx-2" type="button"
                                                        color="success"
                                                        onClick={() => this.acceptTime(record._id)}><FontAwesomeIcon
                                                    icon={faCheckSquare}/><UncontrolledTooltip placement="right"
                                                                                               target={"Tooltip3" + index}>
                                                    Принять
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip9' + index} className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                        target={"Tooltip9" + index}>
                                                    Перенести
                                                </UncontrolledTooltip></Button>

                                            </Fragment>;
                                        }

                                        if (record.status === 'approved') {
                                            status = <Badge color="success">Подтверждена</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltip2' + index} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/><UncontrolledTooltip placement="right"
                                                                                         target={"Tooltip2" + index}>
                                                    Отклонить
                                                </UncontrolledTooltip></Button>
                                                <Button className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/></Button>
                                            </Fragment>
                                        }
                                        if (record.status === 'declined') {
                                            status = <Badge color="secondary">Отклонена</Badge>;
                                            button = null;
                                        }
                                        if (record.status === "rescheduled" && record.doctorConfirm) {
                                            status = <Badge color="warning">Пересена Вами</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltips44' + index} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/>
                                                    <UncontrolledTooltip placement="right"
                                                                         target={"Tooltips44" + index}>
                                                        Отклонить
                                                    </UncontrolledTooltip></Button>
                                            </Fragment>
                                        }
                                        return <tr key={record._id}>
                                            <td>{index + 1}</td>
                                            <td>{record.date}</td>
                                            <td>{record.time}</td>
                                            <td><h5>{status}</h5></td>
                                            <td>
                                                <NavLink tag={RouterNavLink}
                                                         to={'/patient_list/' + record.patientId._id}
                                                         exact
                                                >
                                                    {record.patientId.surname} {record.patientId.name}
                                                </NavLink>
                                            </td>
                                            <td>
                                                {button}
                                            </td>
                                        </tr>
                                    })}
                                    </tbody>
                                </Table>
                            </div>
                            : <h3 className="my-2"> У вас нет записей на эту дату </h3>}
                        {this.props.doctorProfile ?  <Link to={'/booking/' + this.props.doctorProfile._id}>
                            <Button className="m-3" color="primary">
                                Создать запись
                            </Button>
                        </Link> : null}
                        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                            <ModalHeader toggle={this.toggle}>Отмена записи</ModalHeader>
                            <ModalBody>
                                Вы действительно хотите отменить запись?
                            </ModalBody>
                            <ModalFooter>
                                <Button color="danger" onClick={() => this.declineRecord(this.state.id)}>Отменить
                                    запись</Button>{' '}
                                <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
                            </ModalFooter>
                        </Modal>
                        <Modal isOpen={this.props.modalReschedule} toggle={this.toggleRescheduled}
                               className={this.props.className}>
                            <ModalHeader toggle={this.toggleRescheduled}>Выберите дату и время</ModalHeader>
                            <ModalBody>
                                <RescheduleBooking
                                    id={this.state.id}
                                    rescheduleId={this.state.rescheduleId}
                                    data={this.state.data}
                                    date={this.state.date.toLocaleDateString('ru-Ru')}/>
                            </ModalBody>
                        </Modal>
                    </Fragment>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    doctorRecords: state.records.doctorRecords,
    doctorProfile: state.users.doctorProfile,
    websocket: state.users.websocket,
    modalReschedule: state.records.modalReschedule

});

const mapDispatchToProps = dispatch => ({
    fetchDoctorRecords: (doctorId, date) => dispatch(fetchDoctorRecords(doctorId, date)),
    changeRecord: (id, data, date) => dispatch(changeRecord(id, data, date)),
    toggleModal: () => dispatch(openModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorRecords);