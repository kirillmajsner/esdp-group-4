import React from 'react';

const AdminMainPage = () => {
    return (
        <div>
            Добро пожаловать на страницу администратора сайта!
        </div>
    );
};

export default AdminMainPage;