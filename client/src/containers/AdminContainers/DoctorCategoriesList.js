import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader, Table, UncontrolledTooltip} from "reactstrap";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

import {deleteDoctorCategory, fetchDoctorCategoriesAdmin} from "../../store/actions/adminActions";


class DoctorCategoriesList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: ""
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle = (id) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            id: id
        }));
    };


    componentDidMount() {
        this.props.fetchDoctorCategoriesAdmin();
    }

    editCategory = id => {
        this.props.history.push('/admin/doctor_category/' + id + '/edit');
    };

    deleteDoctorCategory = id => {
        this.props.deleteDoctorCategory(id);
        this.toggle();
    };

    render() {
        return (
            <Fragment>
                <Link to='/admin/doctor_category/new'>
                    <Button
                        id="newCategory"
                        className="float-right"
                        color="success"
                    >
                        Добавить новую категорию
                    </Button>
                </Link>

                <h2 className="my-3">Категории врачей</h2>
                <div className="table-container">
                    <Table bordered>
                        <thead>
                        <tr className="d-flex">
                            <th className="col-1">№</th>
                            <th className="col-8">Название</th>
                            <th className="col-3">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.doctorCategories.map((category, index) => {
                            return <tr key={category._id} className="d-flex">
                                <td className="col-1">{index + 1}</td>
                                <td className="col-8">{category.doctorCategory} </td>

                                <td className="col-3">
                                    <Button  id={'Tooltip'} className="mx-2" type="button" color="warning"
                                            onClick={() => this.editCategory(category._id)}><FontAwesomeIcon
                                        icon={faEdit}/><UncontrolledTooltip placement="right" target={"Tooltip"}>
                                        Редактировать
                                    </UncontrolledTooltip></Button>
                                    <Button id={'Tooltips'} className="mx-2" type="button" color="danger"
                                            onClick={() => this.toggle(category._id)}><FontAwesomeIcon icon={faTrash}/>
                                            <UncontrolledTooltip placement="right" target={"Tooltips"}>
                                        Удалить
                                    </UncontrolledTooltip></Button>
                                </td>
                            </tr>
                        })}
                        </tbody>
                    </Table>
                </div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Удаление</ModalHeader>
                    <ModalBody>
                        Вы дейсвительно хотите удалить категорию?
                    </ModalBody>
                    <ModalFooter>
                        <Button id="declineCategory" color="danger"
                                onClick={() => this.deleteDoctorCategory(this.state.id)}>Удалить</Button>{' '}
                        <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
                    </ModalFooter>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.admin.error,
    doctorCategories: state.admin.doctorCategories
});

const mapDispatchToProps = dispatch => ({
    fetchDoctorCategoriesAdmin: () => dispatch(fetchDoctorCategoriesAdmin()),
    deleteDoctorCategory: id => dispatch(deleteDoctorCategory(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorCategoriesList);