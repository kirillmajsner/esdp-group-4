import React, {Component} from 'react';
import {connect} from "react-redux";
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import {loginUser} from "../../store/actions/usersActions";


import FormElement from "../../Components/UI/Form/FormElement";

const root = {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'center',
    width: '80%',
    margin: '20px auto'
};

class AdminLogin extends Component {

    state = {
        isOpen: true,
        password: '',
        email: '',
    };

    handleClose = () => {
        this.setState({isOpen: false});
        this.props.history.push('/')
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        delete this.state.isOpen;
        this.props.loginUser({...this.state}).then(
            () => this.props.history.push('/admin')
        )
    };

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };

    render() {
        const {isOpen, password, email} = this.state;
        return (
            <Modal isOpen={isOpen}
                   toggle={this.handleClose}
                   className={this.props.className}
                   centered={true}
                   size={'lg'}>
                <form style={root} onSubmit={this.submitFormHandler}>
                    <ModalHeader toggle={this.toggle}>Вход</ModalHeader>

                    <ModalBody>
                        <FormElement
                            required
                            propertyName="email"
                            type="email"
                            value={email}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldError('email')}
                            autoComplete="new-email"
                            placeholder="Почтовый адрес"
                        />

                        <FormElement
                            required
                            propertyName="password"
                            type="password"
                            value={password}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldError('password')}
                            autoComplete="new-password"
                            placeholder="Пароль"
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button type="submit" color="primary" className='float-right'>
                            Войти
                        </Button>
                        <Button type="button" color="danger" className='float-left' onClick={this.handleClose}>
                            Отмена
                        </Button>
                    </ModalFooter>
                </form>

            </Modal>
        );
    }
}


const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminLogin);
