import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {createNews} from "../../store/actions/newsActions";
import PostForm from "../../Components/NewsForm/NewsForm";

class CreateNews extends Component {

    createPost = newsData => {
        this.props.onPostCreated(newsData).then(() => {
            this.props.history.push('/');
        });

    };

    componentDidMount() {
        if (!this.props.user) {
            this.props.history.push('/login')
        }

        if (!this.props.user || this.props.user.profile.codeName === 'patient') {
            this.props.history.push('/')
        }
    }

    render() {
        return (
            <Fragment>
                <h2>Добавить новость</h2>
                <PostForm onSubmit={this.createPost}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onPostCreated: newsData => dispatch(createNews(newsData))
});


export default connect(mapStateToProps, mapDispatchToProps)(CreateNews);

