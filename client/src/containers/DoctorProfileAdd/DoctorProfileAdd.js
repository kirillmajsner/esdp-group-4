import React, {Component, Fragment} from 'react';

import {fetchDoctorCategories, fetchClinics, addProfile} from "../../store/actions/profilesActions";
import {connect} from "react-redux";

import DoctorProfileForm from "../../Components/DoctorProfileForm/DoctorProfileForm";

class DoctorProfileAdd extends Component {


    componentDidMount() {
        this.props.fetchDoctorCategories();
        this.props.fetchClinics();
    }

    render() {
        return (
            <Fragment>
                <DoctorProfileForm onSubmit={this.props.addProfile} error={this.props.error}
                doctorCategories={this.props.doctorCategories}
                clinics={this.props.clinics}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error,
    doctorCategories: state.profiles.doctorCategories,
    clinics: state.profiles.clinics
});

const mapDispatchToProps = dispatch => ({
    fetchDoctorCategories: () => dispatch(fetchDoctorCategories()),
    fetchClinics: () => dispatch(fetchClinics()),
    addProfile: (path, profileData) => dispatch(addProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorProfileAdd);
