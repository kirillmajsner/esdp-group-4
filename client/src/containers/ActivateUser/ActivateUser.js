import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {activateUser} from "../../store/actions/usersActions";
import {Alert} from "reactstrap";


class ActivateUser extends Component {


    componentDidMount() {
        this.props.activateUser(this.props.match.params.token);
    }

    render() {
        return (
            <Fragment>
                <Alert color='success'>Активация аккаунта прошла успешно!</Alert>
            </Fragment>
        );
    }
}


const mapDispatchToProps = dispatch => ({
    activateUser: (token) => dispatch(activateUser(token))
});

export default connect(null, mapDispatchToProps)(ActivateUser);

