import React, {Component, Fragment} from 'react';
import {
    Badge,
    Button,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row,
    Table,
    UncontrolledTooltip
} from "reactstrap";
import {connect} from "react-redux";
import {faTrash} from "@fortawesome/free-solid-svg-icons";
import {faCheckSquare} from "@fortawesome/free-solid-svg-icons";
import {faEdit} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import 'moment/locale/ru';

import {changeRecord, fetchPatientRecords, openModal} from "../../store/actions/recordsActions";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";
import RescheduleBooking from "../../Components/RescheduleBooking/RescheduleBooking";

class PatientRecords extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            modalReschedule: false,
            id: "",
            rescheduleId: "",
            data: ""
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle = (id) => {
        this.setState(prevState => ({
            modal: !prevState.modal,
            id
        }));
    };

    toggleRescheduled = (rescheduleId, id) => {
        this.setState(prevState => ({
            modalReschedule: !prevState.modalReschedule,
            rescheduleId: rescheduleId,
            id,
            data: {
                patientConfirm: true,
                status: "rescheduled",
                doctorConfirm: false
            }
        }));
        this.props.openModal();
    };

    componentDidMount() {
        if (this.props.patientProfile) {
            this.props.fetchPatientRecords(this.props.patientProfile._id)
        }
    }

    acceptTime = id => {
        const data = {
            patientConfirm: true,
            status: "approved"
        };

        this.props.changeRecord(id, data);
        this.props.websocket.send(JSON.stringify({type: "CHANGE_RECORD", profile: "patient", id: id}));
    };

    declineRecord = id => {
        const data = {
            patientConfirm: false,
            status: "declined",
        };

        this.props.changeRecord(id, data);
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
        this.props.websocket.send(JSON.stringify({type: "CHANGE_RECORD", profile: "patient", id: id}));
    };

    render() {
        return (
                <Row>
                    <Col xs="12" md="3">
                        <UserCabinetMenu/>
                    </Col>
                    <Col xs="12" md="9">
                        {
                            this.props.patientRecords.length > 0 ?
                        <Fragment>
                            <h2 className="my-3">Мои записи</h2>
                            <div className="table-container">
                                <Table bordered className="DataTable">
                                    <thead>
                                    <tr>
                                        <th scope="row">№</th>
                                        <th scope="row">Дата</th>
                                        <th scope="row">Время</th>
                                        <th scope="row">Статус</th>
                                        <th scope="row">Врач</th>
                                        <th scope="row">Адрес</th>
                                        <th scope="row">Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.props.patientRecords.map((record, index) => {
                                        let status;
                                        let button;
                                        if (record.status === "new") {
                                            status = <Badge color="primary">Отправлено</Badge>;
                                            button =
                                                <Fragment><Button id={'Tooltip3' + index} className="mx-2" type="button"
                                                                  color="danger"
                                                                  onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/>
                                                    <UncontrolledTooltip placement="right" target={"Tooltip3" + index}>
                                                        Отклонить
                                                    </UncontrolledTooltip></Button>
                                                    <Button id={'Tooltip7' + index} className="m-2" type="button"
                                                            color="warning"
                                                            onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                        icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                            target={"Tooltip7" + index}>
                                                        Перенести
                                                    </UncontrolledTooltip></Button>
                                                </Fragment>
                                        }
                                        if (record.status === "new" && record.doctorConfirm) {
                                            status = <Badge color="primary">Отправлено врачом</Badge>;
                                            button =
                                                <Fragment><Button id={'Tooltip3' + index} className="mx-2" type="button"
                                                                  color="danger"
                                                                  onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/>
                                                    <UncontrolledTooltip placement="right" target={"Tooltip3" + index}>
                                                        Отклонить
                                                    </UncontrolledTooltip></Button>
                                                    <Button id={'Tooltip7' + index} className="m-2" type="button"
                                                            color="warning"
                                                            onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                        icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                            target={"Tooltip7" + index}>
                                                        Перенести
                                                    </UncontrolledTooltip></Button>
                                                    <Button id={'Tooltip1' + index} className="m-2" type="button"
                                                            color="success"
                                                            onClick={() => this.acceptTime(record._id)}><FontAwesomeIcon
                                                        icon={faCheckSquare}/><UncontrolledTooltip placement="right"
                                                                                                   target={"Tooltip1" + index}>
                                                        Принять
                                                    </UncontrolledTooltip></Button>
                                                </Fragment>
                                        }
                                        if (record.status === "rescheduled" && record.doctorConfirm) {
                                            status = <Badge color="warning">Перенесена</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltip2' + index} className="m-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/>
                                                    <UncontrolledTooltip placement="right"
                                                                         target={"Tooltip2" + index}>
                                                        Отклонить
                                                    </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip1' + index} className="m-2" type="button"
                                                        color="success"
                                                        onClick={() => this.acceptTime(record._id)}><FontAwesomeIcon
                                                    icon={faCheckSquare}/><UncontrolledTooltip placement="right"
                                                                                               target={"Tooltip1" + index}>
                                                    Принять
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip8' + index} className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                        target={"Tooltip8" + index}>
                                                    Перенести
                                                </UncontrolledTooltip></Button>

                                            </Fragment>;
                                        }
                                        if (record.status === "approved") {
                                            status = <Badge color="success">Подтверждено</Badge>;
                                            button = <Fragment>
                                                <Button id={'Tooltip'} className="mx-2" type="button"
                                                        color="danger"
                                                        onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                    icon={faTrash}/><UncontrolledTooltip placement="right"
                                                                                         target={"Tooltip"}>
                                                    Отклонить
                                                </UncontrolledTooltip></Button>
                                                <Button id={'Tooltip2' + index} className="m-2" type="button"
                                                        color="warning"
                                                        onClick={() => this.toggleRescheduled(record.doctorId._id, record._id)}><FontAwesomeIcon
                                                    icon={faEdit}/><UncontrolledTooltip placement="right"
                                                                                        target={"Tooltip2" + index}>
                                                    Перенести
                                                </UncontrolledTooltip></Button>
                                            </Fragment>
                                        }
                                        if (record.status === "declined") {
                                            status = <Badge color="secondary">Отменено</Badge>;
                                            button = null;

                                        }
                                        if (record.status === "rescheduled" && record.patientConfirm) {
                                            status = <Badge color="warning">Перенесено Вами</Badge>;
                                            button = <Button id={'Tooltip9' + index} className="mx-2" type="button"
                                                             color="danger"
                                                             onClick={() => this.toggle(record._id)}><FontAwesomeIcon
                                                icon={faTrash}/>
                                                <UncontrolledTooltip placement="right" target={"Tooltip9" + index}>
                                                    Отклонить
                                                </UncontrolledTooltip></Button>;
                                        }
                                        return <tr key={record._id}>
                                            <td>{index + 1}</td>
                                            <td className={'records_datetime'}>
                                                {record.date}
                                            </td>
                                            <td className={'records_datetime'}>
                                                {record.time}
                                            </td>
                                            <td><h5>{status}</h5></td>
                                            <td>{record.doctorId.surname} {record.doctorId.name} {record.doctorId.thirdname}
                                            </td>
                                            <td>{record.doctorId.workAddress}</td>
                                            <td>
                                                {button}
                                            </td>
                                        </tr>
                                    })}
                                    </tbody>
                                </Table>
                            </div>
                            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                                <ModalHeader toggle={this.toggle}>Отмена записи</ModalHeader>
                                <ModalBody>
                                    Вы действительно хотите отменить запись?
                                </ModalBody>
                                <ModalFooter>
                                    <Button id="decline" color="danger"
                                            onClick={() => this.declineRecord(this.state.id)}>Отменить
                                        запись</Button>{' '}
                                    <Button color="secondary" onClick={this.toggle}>Закрыть</Button>
                                </ModalFooter>
                            </Modal>
                            <Modal isOpen={this.props.modalReschedule} toggle={this.toggleRescheduled}
                                   className={this.props.className}>
                                <ModalHeader toggle={this.toggleRescheduled}>Выберите дату и время</ModalHeader>
                                <ModalBody>
                                    <RescheduleBooking
                                        id={this.state.id}
                                        rescheduleId={this.state.rescheduleId}
                                        data={this.state.data}/>
                                </ModalBody>
                            </Modal>
                        </Fragment>
                        : <h3>У вас пока нет записей</h3> }
                    </Col>
                </Row>
        );
    }
}

const mapStateToProps = state => ({
    patientRecords: state.records.patientRecords,
    patientProfile: state.users.patientProfile,
    websocket: state.users.websocket,
    modalReschedule: state.records.modalReschedule
});

const mapDispatchToProps = dispatch => ({
    fetchPatientRecords: patientId => dispatch(fetchPatientRecords(patientId)),
    changeRecord: (id, data) => dispatch(changeRecord(id, data)),
    openModal: () => dispatch(openModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientRecords);