import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Card, CardBody, CardFooter, CardText, CardTitle, Col, Row} from "reactstrap";
import {Link} from 'react-router-dom';
import Moment from "react-moment";

import './News.css';
import {fetchNews} from "../../store/actions/newsActions";
import Thumbnail from "../../Components/Thumbnail/Thumbnail";


class News extends Component {


    componentDidMount() {
        this.props.onFetchPosts();
    }

    render() {
        return (
            <Row>
                <Col xs="12">
                    <h2>Новости
                        {!this.props.user || this.props.user &&
                        this.props.user.profile &&
                        this.props.user.profile.codeName &&
                        this.props.user.profile.codeName === 'patient' ? null :
                            <Link to="/news/new">
                                <Button
                                    color="primary"
                                    className="float-right"
                                >
                                    Создать новость
                                </Button>
                            </Link>
                        }
                    </h2>
                    {this.props.news.map(news => (
                        <Card key={news._id} style={{marginBottom: '20px'}}>
                            <CardBody className="News">
                                <Thumbnail image={news.image}/>
                                <div className='newsImage'>
                                    <CardTitle>
                                        <Link id="oneNews" to={'/news/' + news._id}>
                                            {news.title}
                                        </Link>
                                    </CardTitle>
                                    <CardText className="descriptionNews">
                                        {news.description}
                                    </CardText>
                                </div>
                            </CardBody>
                            <CardFooter style={{backgroundColor: '#009b5c', color: "#fff"}}><Moment
                                format="DD.MM.YYYY">{news.datetime}</Moment></CardFooter>
                        </Card>
                    ))}
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    news: state.news.news,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchPosts: () => dispatch(fetchNews())
});

export default connect(mapStateToProps, mapDispatchToProps)(News);
