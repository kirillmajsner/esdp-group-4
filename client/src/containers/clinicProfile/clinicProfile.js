import React, {Component} from 'react';
import {connect} from "react-redux";
import ClinicProfileItem from "../../Components/ClinicProfileItem/ClinicProfileItem";
import {fetchProfileDoctorClinic} from "../../store/actions/profilesActions";
import {Col, Row} from "reactstrap";

class ClinicProfile extends Component {
    componentDidMount() {
        this.props.fetchProfileDoctorClinic();
    }

    render() {
        return (
            this.props.clinicProfiles ? <div>
                <h3>Список клиник</h3>
                <Row>
                {this.props.clinicProfiles.map(clinic => (
                        <Col sm="6"
                             key={clinic._id}
                        >
                            <ClinicProfileItem
                                key={clinic._id}
                                _id={clinic._id}
                                title={clinic.title}
                                description={clinic.description}
                                address={clinic.address}
                                phone={clinic.phone}
                                image={clinic.user.image}
                            />
                        </Col>
                ))}
                </Row>
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    clinicProfiles: state.profiles.clinicProfiles,
});

const mapDispatchToProps = dispatch => ({
    fetchProfileDoctorClinic: () => dispatch(fetchProfileDoctorClinic()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ClinicProfile);