import React, {Component} from 'react';
import {
    Button,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader
} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";
import FormElement from "../../Components/UI/Form/FormElement";
import {connect} from "react-redux";
import {fetchProfiles} from "../../store/actions/profilesActions";

const root = {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'center',
    width: '80%',
    margin: '20px auto'
};

class RegisterUser extends Component {

    state = {
        isOpen: true,
        username: '',
        password: '',
        email: '',
        profile: ''
    };

    componentDidMount() {
        this.props.fetchProfiles();
    }

    handleClose = () => {
        this.setState({isOpen: false});
        this.props.history.push('/')
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();
        this.props.registerUser({...this.state});
    };


    getFieldError = fieldName => {
        const {error} = this.props
        return (
            error &&
            error &&
            error[fieldName] &&
            error[fieldName].message
        )
    };

    render() {
        const {isOpen, password, email, profile} = this.state;
        const {className, profiles} = this.props
        return (
            <Modal isOpen={isOpen}
                   className={className}
                   centered={true}
                   toggle={this.handleClose}
                   size="lg">
                <form style={root} onSubmit={this.submitFormHandler}>
                    <ModalHeader>Регистрация</ModalHeader>

                    <ModalBody>
                        <FormElement
                            propertyName="email"
                            type="email"
                            value={email}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldError('email')}
                            autoComplete="new-email"
                            placeholder="Введите ваш почтовый адрес "
                        />
                        <FormElement
                            type="select"
                            propertyName="profile" id="profile"
                            value={profile}
                            onChange={this.inputChangeHandler}
                        >
                            <option value="" disabled>Выберите профиль</option>
                            {profiles.map(profile => (
                                <option key={profile._id} value={profile._id}>{profile.title}</option>
                            ))}
                        </FormElement>
                        <FormElement
                            propertyName="password"
                            type="password"
                            value={password}
                            onChange={this.inputChangeHandler}
                            error={this.getFieldError('password')}
                            autoComplete="new-password"
                            placeholder="Введите пароль"
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button type="button" color="danger" className='float-left' onClick={this.handleClose}>
                            Отмена
                        </Button>
                        <Button type="submit" color="primary" className='float-right'>
                            Регистрация
                        </Button>
                    </ModalFooter>
                </form>

            </Modal>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError,
    profiles: state.profiles.profiles
});

const mapDispatchToProps = dispatch => ({
    fetchProfiles: () => dispatch(fetchProfiles()),
    registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterUser);
