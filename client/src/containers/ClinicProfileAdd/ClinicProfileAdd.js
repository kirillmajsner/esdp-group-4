import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import ClinicProfileForm from "../../Components/ClinicProfileForm/ClinicProfileForm";
import {addProfile} from "../../store/actions/profilesActions";



class ClinicProfileAdd extends Component {

    render() {
        return (
            <Fragment>
                <ClinicProfileForm onSubmit={this.props.addProfile} error={this.props.error}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error
});

const mapDispatchToProps = dispatch => ({
    addProfile: (path, profileData) => dispatch(addProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(ClinicProfileAdd);
