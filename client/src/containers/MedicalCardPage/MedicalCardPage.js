import React, {Component} from 'react';
import {fetchCardInfo, fetchQuestionnaire,} from "../../store/actions/medicalCards";
import {ClipLoader} from 'react-spinners';
import {NavLink as RouterNavLink} from "react-router-dom";

import './MedicalCardPage.css';

import {connect} from "react-redux";
import {Button, Col, NavLink, Row} from "reactstrap";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";

class MedicalCardPage extends Component {

    componentDidMount() {
        this.props.fetchCardInfo(this.props.match.params.cardId);
        this.props.fetchQuestionnaire(this.props.match.params.id)
    }

    render() {
        const {questionnaire, cardInfo} = this.props;
        const quest = questionnaire && questionnaire[0];
        return (
            <React.Fragment>
                <Row>
                    <Col xs="12" md="3">
                        <UserCabinetMenu/>
                    </Col>
                    <Col xs="12" md="9">
                        {!this.props.cardInfo ?
                            <div className={'cardPage_loader'}>
                                <ClipLoader
                                    sizeUnit={"px"}
                                    size={60}
                                    margin={'2px'}
                                    color={'#07019b'}
                                    loading={true}
                                />
                            </div>
                            :
                            <div>
                                <div>
                                    <div className={'cardPage_questionnaire-block'}>
                                        <h2>Данные анкеты: </h2>
                                        {
                                            quest &&
                                            <Button
                                                className='float-lg-right'
                                                size={'sm'}
                                                outline
                                                color="primary"
                                            >
                                                <NavLink tag={RouterNavLink}
                                                         to={`/patient_list/${this.props.match.params.id}/questionnaire/edit/${quest._id}`}
                                                         exact
                                                >
                                                    Изменить анкету
                                                </NavLink>
                                            </Button>
                                        }

                                    </div>
                                    <ul>
                                        {
                                            quest &&
                                            quest.questionnaire &&
                                            questionnaire[0].questionnaire.map((item, key) => (
                                                <li key={key}>
                                                    {item}
                                                </li>
                                            ))
                                        }
                                    </ul>
                                </div>
                                <div className={'cardPage_info-block'}>
                                    <h3>Диагноз: </h3>
                                    <p>{cardInfo.diagnosis}</p>
                                    <h3>Перенесенные и сопутствующие заболевания: </h3>
                                    <p>{cardInfo.pastIllnesses ? cardInfo.pastIllnesses : '-'}</p>
                                    <h3>Развитие настоящего заболевания: </h3>
                                    <p>{cardInfo.presentDisease ? cardInfo.presentDisease : '-'}</p>
                                </div>
                            </div>


                        }
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    cardInfo: state.medicalCards.cardInfo,
    questionnaire: state.medicalCards.questionnaire
});

const mapDispatchToProps = (dispatch) => ({
    fetchCardInfo: id => dispatch(fetchCardInfo(id)),
    fetchQuestionnaire: id => dispatch(fetchQuestionnaire(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MedicalCardPage);