import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PatientProfileForm from "../../Components/PatientProfileForm/PatientProfileForm";
import {addProfile} from "../../store/actions/profilesActions";



class PatientProfileAdd extends Component {

    addProfile = data => {
        this.props.addProfile(data);
    };

    render() {
        return (
            <Fragment>
                <PatientProfileForm onSubmit={this.props.addProfile} error={this.props.error}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error
});

const mapDispatchToProps = dispatch => ({
    addProfile: (path, profileData) => dispatch(addProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientProfileAdd);
