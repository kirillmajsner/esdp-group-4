import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PatientProfileForm from "../../Components/PatientProfileForm/PatientProfileForm";
import {editProfile} from "../../store/actions/profilesActions";


class PatientProfileEdit extends Component {

    editProfile = data => {
        this.props.editProfile(data);
    };

    render() {
        return (
            <Fragment>
                <PatientProfileForm onSubmit={this.props.editProfile}
                                    error={this.props.error}
                                    data={this.props.patientProfile}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error,
    patientProfile: state.users.patientProfile
});

const mapDispatchToProps = dispatch => ({
    editProfile: (path, profileData) => dispatch(editProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(PatientProfileEdit);
