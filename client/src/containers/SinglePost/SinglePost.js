import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Alert, Button, Card, CardBody, CardFooter, Col, Form, Input, Row} from "reactstrap";

import {createComment, deleteComment, fetchComments} from "../../store/actions/commentActions";
import Thumbnail from "../../Components/Thumbnail/Thumbnail";
import {fetchOneNews} from "../../store/actions/newsActions";

import './SinglePost.css'

class SinglePost extends Component {

    state = {
        description: '',
        news: this.props.match.params.id
    };

    inputChangeHandler = event => {
        this.setState({[event.target.name]: event.target.value})
    };

    submitChangeHandler = event => {
        event.preventDefault();
        this.props.createComment({...this.state}, this.props.match.params.id)
        this.setState({description: ''})
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchOnePost(id);
        this.props.fetchComments(id);
    }


    render() {
        const {user} = this.props
        return (
            <Fragment>
                {this.props.onePost ? <Row style={{margin: '20px'}}>
                    <Col sm='12'>
                        <Card>
                            <CardBody className="SinglePost">
                                <Thumbnail image={this.props.onePost.image}/>
                                <h2>{this.props.onePost.title}</h2>
                                <p>{this.props.onePost.description}</p>
                            </CardBody>
                        </Card>
                    </Col>
                </Row> : null}

                {!this.props.comments || this.props.comments.length <= 0 ? null :
                    <div>
                        <h2 className={'comment-title'}>Комментарии:</h2>
                        {this.props.comments.map((item) => {
                            return (
                                <Card key={item._id}
                                      style={{margin: '20px'}}>
                                    <CardFooter>Автор: {item.user.email}</CardFooter>
                                    <CardBody>{item.description}</CardBody>
                                    {
                                        user &&
                                        user._id === item.user._id &&
                                        <div style={{padding: '20px'}} className="delete-button ml-auto">
                                            <Button
                                                id="deleteComment"
                                                color="danger"
                                                onClick={() => this.props.deleteComment(item._id, item.news._id)}
                                            >
                                                Удалить
                                            </Button>
                                        </div>
                                    }
                                </Card>
                            )
                        })}
                    </div>
                }

                {!this.props.user
                    ? <Alert color='danger'>Зарегистрируйтесь чтобы оставить комментарий</Alert>
                    : <Form className={'add-comment'}>
                    <h3>Хотите добавить комметарий?</h3>
                    <Input type='text'
                           id="comment"
                           name='description'
                           value={this.state.description}
                           onChange={this.inputChangeHandler}/>
                    <Button id="addComment" color="primary" style={{margin: '20px'}}
                            onClick={this.submitChangeHandler}>Добавить</Button>
                </Form>}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    comments: state.comments.comments,
    onePost: state.news.onePost,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchComments: id => dispatch(fetchComments(id)),
    fetchOnePost: id => dispatch(fetchOneNews(id)),
    createComment: (data, id) => dispatch(createComment(data, id)),
    deleteComment: (id, newsId) => dispatch(deleteComment(id, newsId))
});

export default connect(mapStateToProps, mapDispatchToProps)(SinglePost);
