import React, {Component, Fragment} from 'react';

import {fetchDoctorCategories, fetchClinics, editProfile} from "../../store/actions/profilesActions";
import {connect} from "react-redux";

import DoctorProfileForm from "../../Components/DoctorProfileForm/DoctorProfileForm";

class DoctorProfileEdit extends Component {


    componentDidMount() {
        this.props.fetchDoctorCategories();
        this.props.fetchClinics();
    }


    editProfile = data => {
        this.props.editProfile(data);
    };

    render() {
        return (
            <Fragment>
                <DoctorProfileForm onSubmit={this.props.editProfile} error={this.props.error}
                                   data={this.props.doctorProfile}
                                   doctorCategories={this.props.doctorCategories}
                                   clinics={this.props.clinics}/>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.profiles.error,
    doctorCategories: state.profiles.doctorCategories,
    clinics: state.profiles.clinics,
    doctorProfile: state.users.doctorProfile
});

const mapDispatchToProps = dispatch => ({
    fetchDoctorCategories: () => dispatch(fetchDoctorCategories()),
    fetchClinics: () => dispatch(fetchClinics()),
    editProfile: (path, profileData) => dispatch(editProfile(path, profileData))
});

export default connect(mapStateToProps, mapDispatchToProps)(DoctorProfileEdit);
