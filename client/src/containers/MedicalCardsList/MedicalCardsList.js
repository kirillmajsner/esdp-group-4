import React, {Component} from 'react';
import {Button, Card, CardTitle, Col, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import './MedicalCardsList.css'
import {fetchMedicalCards, fetchPatientId} from "../../store/actions/medicalCards";
import {connect} from "react-redux";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";


class MedicalCardsList extends Component {

    componentDidMount() {
        const id = this.props.match.params.id
        this.props.fetchPatientId(id)
        this.props.fetchMedicalCards(id)
    }

    dateFormat = date => {
        let d = new Date(date);
        return <p> {d.toLocaleDateString('ru-GB')}  {d.toLocaleTimeString()}</p>;
    };

    render() {

        const {medicalCards, cards} = this.props
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Button color='primary' size={'sm'}>
                        <NavLink tag={RouterNavLink}
                                 to={'/patient_list/' + this.props.match.params.id + '/medical_card'}
                                 exact
                                 className={'record_navlink'}
                        >
                            Добавить запись
                        </NavLink>
                    </Button>

                    <Col xs="12" md="12">
                        <div>
                            <h2 style={{textAlign: 'center'}}>
                                {`${medicalCards && medicalCards.surname}
                            ${medicalCards && medicalCards.name}
                            ${medicalCards && medicalCards.thirdname}`}
                            </h2>
                        </div>
                        <div>
                            <h2 style={{textAlign: 'center'}}>
                                {`${medicalCards && medicalCards.gender === 'male' ? 'Мужчина' : 'Женщина'}
                            ${medicalCards && medicalCards.dateOfBirth}
                            ${medicalCards && medicalCards.workAddress}
                            ${medicalCards && medicalCards.residence}`}
                            </h2>
                        </div>
                    </Col>
                    {
                        cards && cards.length ? cards.map(item => (
                                <Col xs={12} md={4} key={item._id}>
                                    <Card body className={'medicalCards-card'}>
                                        <CardTitle>
                                            <span style={{paddingRight: '5px'}}>Медицинская карта от:</span>
                                            {item.date
                                                ? this.dateFormat(item.date)

                                                : 'дата не определена'}
                                        </CardTitle>
                                        <Button color={'info'} size={'sm'}>
                                            <NavLink tag={RouterNavLink}
                                                     className={'medicalCard_navLink'}
                                                     to={`/patient_list/${this.props.match.params.id}/view/${item._id}`}
                                                     exact
                                            >
                                                Смотреть
                                            </NavLink>
                                        </Button>
                                    </Card>
                                </Col>
                            ))
                            : null
                    }
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    medicalCards: state.medicalCards.patient,
    cards: state.medicalCards.medicalCards,
})

const mapDispatchToProps = (dispatch) => ({
    fetchPatientId: data => dispatch(fetchPatientId(data)),
    fetchMedicalCards: id => dispatch(fetchMedicalCards(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(MedicalCardsList);