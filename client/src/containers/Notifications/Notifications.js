import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Col, Row, Table} from "reactstrap";
import UserCabinetMenu from "../../Components/UserCabinetMenu/UserCabinetMenu";
import {changeNotificationStatus, fetchNotifications,} from "../../store/actions/notificationsActions";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock} from "@fortawesome/free-solid-svg-icons";
import {faCalendarWeek} from "@fortawesome/free-solid-svg-icons";
import {Link} from "react-router-dom";

import './Notifications.css';

class Notifications extends Component {

    componentDidMount() {
        if (this.props.doctorProfile) {
            this.props.fetchNotifications(this.props.doctor._id, "doctor");
        } if(this.props.patientProfile){
            this.props.fetchNotifications(this.props.patient._id, "patient");
        }

    }

    dateFormat = date => {
        let d = new Date(date);
        return <p><FontAwesomeIcon
            icon={faCalendarWeek}/> {d.toLocaleDateString('ru-GB')} <br/>
            <FontAwesomeIcon icon={faClock}/> {d.toLocaleTimeString()}</p>;
    };

    changeStatus = id => {
        let data;
        if (this.props.user.profile.codeName === "doctor") {
            data = {
                doctorStatus: "read"
            };
        } else {
            data = {
                patientStatus: "read"
            };
        }

        this.props.changeNotificationStatus(id, data);
    };

    render() {
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Fragment>
                        {this.props.notifications.length > 0 ?
                            <div className="table-container">
                                <Table bordered>
                                    <thead>
                                    <tr>
                                        <th scope="row">№</th>
                                        <th scope="row">Дата</th>
                                        <th scope="row">Текст</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this.props.notifications.map((message, index) => {
                                        return <tr key={message._id} >
                                            <td>{index + 1}</td>
                                            <td>{this.dateFormat(message.datetime)} </td>
                                            <td><p onClick={() => this.changeStatus(message._id)}>
                                                {this.props.patient ?
                                                    <Link className={message.patientStatus === "new" ? "New" : "Read"}
                                                          to={'/notifications/' + message._id}>{message.notification}</Link>
                                                    : <Link className={message.doctorStatus === "new" ? "New" : "Read"}
                                                            to={'/notifications/' + message._id}>{message.notification}</Link>}
                                            </p></td>
                                        </tr>
                                    })}
                                    </tbody>
                                </Table>
                            </div>
                            : <h3>У вас нет уведомлений</h3>}
                    </Fragment>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    doctor: state.users.doctorProfile,
    patient: state.users.patientProfile,
    user: state.users.user,
    notifications: state.notification.notifications
});

const mapDispatchToProps = dispatch => ({
    fetchNotifications: (id, path) => dispatch(fetchNotifications(id, path)),
    changeNotificationStatus: (id, data) => dispatch(changeNotificationStatus(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
