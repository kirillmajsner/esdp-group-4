import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {
    Button,
    Card,
    CardBody,
    CardSubtitle,
    CardText,
    Col,
    Modal,
    ModalBody,
    ModalFooter,
    NavLink,
    Row
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import Moment from "react-moment";
import Rating from "react-rating";
import Calendar from 'react-calendar';

import {fetchDoctorPublic} from "../../store/actions/profilesActions";
import Thumbnail from "../../Components/Thumbnail/Thumbnail";

import './OneDoctor.css';






class OneDoctor extends Component {

    state = {
        doctor: this.props.match.params.id,
        selectedItem: null,
        date: new Date(),
    };


    hideModal = () => {
        this.setState({selectedItem: null})
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.fetchDoctor(id);
    }

    averageRating = data => {
        const ratingsNum = data.length;
        const sum = data.reduce((acc, currentVal) => acc += currentVal.rating || 0, 0);
        return (sum === 0 && ratingsNum === 0) ? 0 : Math.round(sum / ratingsNum);

    };

    render() {
        return (
            <Fragment>
                {this.props.doctor ? <Row>
                    <Col xs='12'>
                        <div className="container">
                            <div className="doctor-img">
                                <Thumbnail image={this.props.doctor.user.image}/>
                            </div>
                            <div className="doctor-info">
                                <h1>{this.props.doctor.name} {this.props.doctor.surname}</h1>
                                <h6><i>{this.props.doctor.speciality}</i></h6>
                                <p>Место
                                    работы: {this.props.doctor.clinic.title} / {this.props.doctor.clinic.address}</p>
                                <p>Номер телефона: {this.props.doctor.phone}</p>
                                <p>Стаж: {this.props.doctor.yearsOfWork} лет</p>
                                <p><Rating readonly
                                           stop={10}
                                           placeholderRating={this.averageRating(this.props.doctor.rating)}/></p>
                                <p>Количество оценок {this.props.doctor.rating.length}</p>
                                <p>Средняя оценка: {this.averageRating(this.props.doctor.rating)}</p>
                                <p><span className='doctor-price_price'>От {this.props.doctor.price}</span> сом</p>
                                {this.props.patientProfile || this.props.doctorProfile ? <Button color="success">
                                        <NavLink tag={RouterNavLink}
                                                 to={'/booking/' + this.props.doctor._id}
                                                 exact
                                                 className={'record_navlink'}
                                        >
                                            Записаться
                                        </NavLink>
                                    </Button>
                                    : null}
                            </div>
                        </div>

                        <Modal size='lg' isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                            {this.state.selectedItem && (
                                <Fragment>
                                    <ModalBody>
                                        <Calendar className='float-left'
                                                  onChange={this.onChange}
                                                  value={this.state.date}
                                        />
                                        <textarea className='textarea' rows="12" cols="40" name="text"/>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                                    </ModalFooter>
                                </Fragment>
                            )}

                        </Modal>
                    </Col>
                </Row> : null}


                {!this.props.doctor ?
                    <div className='text-center my-4'><h1>Отзывов пока нет</h1>
                        <p>Отзывы могут оставить только клиенты,
                            лечившиеся у врача</p>
                    </div> :
                    <Col xs="12">
                        <div className='text-center'>
                            <h1>Отзывы</h1>
                            <p>Отзывы и рейтинг могут выставить только пациенты врача</p>
                        </div>
                        {this.props.doctor.rating.map((item) => {
                            return (
                                <Card key={item._id} className='reviews'>
                                    <div className='name'>
                                        <CardSubtitle>{!item.anonymous ? item.surname + " " + item.name : "Аноним"}</CardSubtitle>
                                    </div>

                                    <CardBody>
                                        <CardText>{item.review}</CardText>
                                        <CardText><Moment format="D MMM YYYY HH:mm"
                                                          withTitle>{item.datetime}</Moment></CardText>
                                        <CardText>Оценка пользователя: <strong>{item.rating}</strong></CardText>
                                    </CardBody>

                                </Card>
                            )
                        })}

                    </Col>
                }
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    doctor: state.profiles.doctorPublic,
    user: state.users.user,
    patientProfile: state.users.patientProfile,
    doctorProfile: state.users.doctorProfile
});

const mapDispatchToProps = dispatch => ({
    fetchDoctor: id => dispatch(fetchDoctorPublic(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OneDoctor);
