export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const ADD_DATA_REQUEST = 'ADD_DATA_REQUEST';
export const ADD_DATA_FAILURE = 'ADD_DATA_FAILURE';
export const ADD_DATA_SUCCESS = 'ADD_DATA_SUCCESS';

export const DELETE_DATA_REQUEST = 'DELETE_DATA_REQUEST';
export const DELETE_DATA_FAILURE = 'DELETE_DATA_FAILURE';
export const DELETE_DATA_SUCCESS = 'DELETE_DATA_SUCCESS';

export const FETCH_PROFILES_SUCCESS = 'FETCH_PROFILES_SUCCESS';

export const FETCH_DOCTORS_SUCCESS = 'FETCH_DOCTORS_SUCCESS';
export const FETCH_DOCTOR_SUCCESS = 'FETCH_DOCTOR_SUCCESS';

export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CLINICS_LIST_SUCCESS = 'FETCH_CLINICS_LIST_SUCCESS';

export const DELETE_DATA_AFTER_LOGOUT = "DELETE_DATA_AFTER_LOGOUT";

export const FETCH_DOCTOR_PROFILE_SUCCESS = "FETCH_DOCTOR_PROFILE_SUCCESS";
export const FETCH_SHOP_PROFILE_SUCCESS = "FETCH_SHOP_PROFILE_SUCCESS";
export const FETCH_CLINIC_PROFILE_SUCCESS = "FETCH_CLINIC_PROFILE_SUCCESS";
export const FETCH_PATIENT_SUCCESS = 'FETCH_PATIENT_SUCCESS';
export const FETCH_PROFILE_FAILURE = 'FETCH_PROFILE_FAILURE';

export const FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS = "FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS";
export const FETCH_CLINIC_DOCTORS_SUCCESS = "FETCH_CLINIC_DOCTORS_SUCCESS";
export const FETCH_CLINIC_INFO_SUCCESS = 'FETCH_CLINIC_INFO_SUCCESS';


export const SEARCH_DATA = "SEARCH_DATA";
export const FETCH_PATIENT_FOR_RECORD = "FETCH_PATIENT_FOR_RECORD";