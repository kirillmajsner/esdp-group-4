import axios from "../../axios";
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const CREATE_NEWS_SUCCESS = 'CREATE_NEWS_SUCCESS';
export const FETCH_ONE_NEWS = 'FETCH_ONE_NEWS';


export const fetchNewsSuccess = news => ({type: FETCH_NEWS_SUCCESS, news});
export const fetchOneNewsSuccess = post => ({type: FETCH_ONE_NEWS, post});
export const createNewsSuccess = () => ({type: CREATE_NEWS_SUCCESS});

export const fetchOneNews = (id) => {
    return dispatch => {
        return axios.get(`/news/${id}`).then(response => {
            dispatch(fetchOneNewsSuccess(response.data));
        })
    }
};

export const fetchNews = () => {
    return dispatch => {
        return axios.get('/news').then(
            response => dispatch(fetchNewsSuccess(response.data))
        );
    };
};

export const createNews = (newsData) => {
    return dispatch => {
        return axios.post('/news', newsData).then(
            response => {
                dispatch(createNewsSuccess(response.data));
                NotificationManager.success(response.data.message);
                dispatch(push('/news'))
            }
        )
    }
};