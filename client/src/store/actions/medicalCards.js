import axios from '../../axios';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FETCH_PATIENT_ID_SUCCESS = 'FETCH_PATIENT_ID_SUCCESS';
export const FETCH_MEDICAL_CARDS_SUCCESS = 'FETCH_MEDICAL_CARDS_SUCCESS';

export const FETCH_CARD_INFO_SUCCESS = 'FETCH_CARD_INFO_SUCCESS';

export const FETCH_QUESTIONNAIRE_SUCCESS = 'FETCH_QUESTIONNAIRE_SUCCESS';

const fetchPatientIdSuccess = data => ({type: FETCH_PATIENT_ID_SUCCESS, data});

const fetchMedicalCardsSuccess = data => ({type: FETCH_MEDICAL_CARDS_SUCCESS, data});

const fetchCardInfoSuccess = data => ({type: FETCH_CARD_INFO_SUCCESS, data});

const fetchQuestionnaireSuccess = data => ({type: FETCH_QUESTIONNAIRE_SUCCESS, data});

export const fetchPatientId = id => {
    return dispatch => {
        return axios.get('/medical_cards/' + id).then(
            response => dispatch(fetchPatientIdSuccess(response.data))
        )
    }
};
export const createMedicalCard = (patientId, data) => {
    return dispatch => {
        return axios.post('/medical_cards?patient=' + patientId, data).then(
            response => NotificationManager.success(response.data.message)
        )
    }
};

export const createQuestionnaire = (patientId, data) => {
    return dispatch => {
        return axios.post('/questionnaire?patient=' + patientId, data).then(
            response => NotificationManager.success(response.data.message)
        ).then(() => {
            dispatch(push('/'))
        })
    }
};

export const fetchMedicalCards = (patientId) => {
    return dispatch => {
        return axios.get('/medical_cards?patient=' + patientId).then(
            response => dispatch(fetchMedicalCardsSuccess(response.data))
        )
    }
};

export const fetchCardInfo = id => {
    return dispatch => {
        return axios.get('/medical_cards/view/' + id).then(
            response => dispatch(fetchCardInfoSuccess(response.data))
        )
    }
};

export const fetchQuestionnaire = id => {
    return dispatch => {
        return axios.get('/questionnaire/' + id).then(
            response => dispatch(fetchQuestionnaireSuccess(response.data))
        )
    }
};

export const editQuestionnaire = (id, data) => {
    return dispatch => {
        return axios.put('/questionnaire/edit/' + id, data).then(
            response => NotificationManager.success(response.data.message)
        ).then(() => {
            dispatch(push('/'))
        })
    }
};
