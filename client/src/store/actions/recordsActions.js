import axios from "../../axios";
import {NotificationManager} from "react-notifications";

export const FETCH_PATIENT_RECORDS_SUCCESS = 'FETCH_PATIENT_RECORDS_SUCCESS';
export const FETCH_DOCTOR_RECORDS_SUCCESS = 'FETCH_DOCTOR_RECORDS_SUCCESS';
export const FETCH_RECORDS_FAILURE = "FETCH_RECORDS_FAILURE";

export const DELETE_RECORDS_AFTER_LOGOUT = "DELETE_RECORDS_AFTER_LOGOUT";

export const CHANGE_RECORD_SUCCESS = 'CHANGE_RECORD_SUCCESS';
export const CHANGE_RECORD_FAILURE = "CHANGE_RECORD_FAILURE";
export const OPEN_MODAL = "OPEN_MODAL";


const fetchPatientRecordSuccess = data => {
    return {type: FETCH_PATIENT_RECORDS_SUCCESS, data};
};

const fetchDoctorRecordSuccess = data => {
    return {type: FETCH_DOCTOR_RECORDS_SUCCESS, data};
};

export const deleteRecordsAfterLogout = () => {
    return {type: DELETE_RECORDS_AFTER_LOGOUT};
};

const fetchRecordFailure = error => ({type: FETCH_RECORDS_FAILURE, error});

const changeRecordSuccess = data => {
    return {type: CHANGE_RECORD_SUCCESS, data};
};
const changeRecordFailure = error => ({type: CHANGE_RECORD_FAILURE, error});

export const openModal = () => {
    return {type: OPEN_MODAL};
};
export const fetchPatientRecords = (patientId) => {
    return dispatch => {
        if (!patientId) {
            NotificationManager.warning('Заполните анкету пациента');
        } else {
            return axios.get('/records?patientId=' + patientId).then(
                response => dispatch(fetchPatientRecordSuccess(response.data)),
                error => dispatch(fetchRecordFailure(error))
            );
        }
    };
};

export const fetchDoctorRecords = (doctorId, date) => {
    return dispatch => {
        if (!doctorId) {
            NotificationManager.warning('Заполните анкету доктора');
        } else {
            return axios.get('/records?doctorId=' + doctorId + '&date=' + date).then(
                response => dispatch(fetchDoctorRecordSuccess(response.data)),
                error => dispatch(fetchRecordFailure(error))
            );
        }
    };
};

export const changeRecord = (id, data, date) => {
    return (dispatch, getState) => {
        let state = getState();
        return axios.put('/records/' + id, data).then(
            response => {
                dispatch(changeRecordSuccess(response.data));
                state.users.patientProfile ? dispatch(fetchPatientRecords(state.users.patientProfile._id))
                   : dispatch(fetchDoctorRecords(state.users.doctorProfile._id, date))
            },
            error => dispatch(changeRecordFailure(error))
        );
    };
};
