import axios from '../../axios';
import {NotificationManager} from "react-notifications";

export const FETCH_NOTIFICATIONS_SUCCESS = 'FETCH_NOTIFICATIONS_SUCCESS';
export const FETCH_NOTIFICATIONS_FAILURE = "FETCH_NOTIFICATIONS_FAILURE";

export const FETCH_ONE_NOTIFICATION_SUCCESS = 'FETCH_ONE_NOTIFICATION_SUCCESS';
export const DELETE_NOTIFICATIONS_AFTER_LOGOUT = "DELETE_NOTIFICATIONS_AFTER_LOGOUT";
export const TOGGLE_WEBSOCKET = "TOGGLE_WEBSOCKET";

const fetchNotificationSuccess = data => {
    return {type: FETCH_NOTIFICATIONS_SUCCESS, data};
};

export const deleteNotificationsAfterLogout = () => {
    return {type: DELETE_NOTIFICATIONS_AFTER_LOGOUT};
};

const fetchOneNotificationSuccess = data => {
    return {type: FETCH_ONE_NOTIFICATION_SUCCESS, data};
};

export const toggleWebsocket = () => {
    return {type: TOGGLE_WEBSOCKET}
};

const fetchNotificationsFailure = error => ({type: FETCH_NOTIFICATIONS_FAILURE, error});

export const fetchNotifications = (id, path) => {
    return dispatch => {
        if (!id) {
            NotificationManager.warning('Заполните анкету');
        } else {
            return axios.get('/notifications?' + path + "=" + id).then(
                response => dispatch(fetchNotificationSuccess(response.data)),
                error => dispatch(fetchNotificationsFailure(error))
            );
        }
    };
};

export const fetchOneNotifications = (id) => {
    return dispatch => {
            return axios.get('/notifications/' + id).then(
                response => dispatch(fetchOneNotificationSuccess(response.data)),
                error => dispatch(fetchNotificationsFailure(error))
            );
    };
};

export const changeNotificationStatus = (id, data) => {
    return dispatch => {
        return axios.put('/notifications/' + id, data).then(
            error => dispatch(fetchNotificationsFailure(error))
        );
    };
};