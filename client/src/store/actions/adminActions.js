import axios from "../../axios";
import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";

export const ADMIN_TOGGLE_APPROVED_SUCCESS = 'ADMIN_TOGGLE_APPROVED_SUCCESS';
export const ADMIN_TOGGLE_APPROVED_FAILURE = "ADMIN_TOGGLE_APPROVED_FAILURE";

export const ADMIN_FETCH_DOCTORS_SUCCESS = 'ADMIN_FETCH_DOCTORS_SUCCESS';
export const ADMIN_FETCH_DOCTORS_FAILURE = "ADMIN_FETCH_DOCTORS_FAILURE";

export const ADMIN_FETCH_DOCTOR_CATEGORIES_SUCCESS = 'ADMIN_FETCH_DOCTOR_CATEGORIES_SUCCESS';
export const ADMIN_FETCH_DOCTOR_CATEGORIES_FAILURE = "ADMIN_FETCH_DOCTOR_CATEGORIES_FAILURE";

export const ADMIN_FETCH_DOCTOR_CATEGORY_SUCCESS = 'ADMIN_FETCH_DOCTOR_CATEGORY_SUCCESS';
export const ADMIN_FETCH_DOCTOR_CATEGORY_FAILURE = "ADMIN_FETCH_DOCTOR_CATEGORY_FAILURE";

export const ADMIN_ADD_SUCCESS = 'ADMIN_ADD_SUCCESS';
export const ADMIN_ADD_FAILURE = "ADMIN_ADD_FAILURE";

export const ADMIN_FETCH_CLINICS_SUCCESS = 'ADMIN_FETCH_CLINICS_SUCCESS';
export const ADMIN_FETCH_CLINICS_FAILURE = "ADMIN_FETCH_CLINICS_FAILURE";

export const ADMIN_FETCH_CLINIC_ID_SUCCESS = 'ADMIN_FETCH_CLINIC_ID_SUCCESS';
export const ADMIN_FETCH_CLINIC_ID_FAILURE = "ADMIN_FETCH_CLINIC_ID_FAILURE";

export const ADMIN_FETCH_DOCTOR_ID_SUCCESS = 'ADMIN_FETCH_DOCTOR_ID_SUCCESS';
export const ADMIN_FETCH_DOCTOR_ID_FAILURE = "ADMIN_FETCH_DOCTOR_ID_FAILURE";

export const ADMIN_DELETE_SUCCESS = 'ADMIN_DELETE_SUCCESS';
export const ADMIN_DELETE_FAILURE = "ADMIN_DELETE_FAILURE";

export const FETCH_ADMIN_NOTIFICATIONS_SUCCESS = 'FETCH_ADMIN_NOTIFICATIONS_SUCCESS';
export const FETCH_ADMIN_NOTIFICATIONS_FAILURE = "FETCH_ADMIN_NOTIFICATIONS_FAILURE";

export const ADMIN_TOGGLE_WEBSOCKET = "ADMIN_TOGGLE_WEBSOCKET";

const fetchDoctorsSuccess = data => {
    return {type: ADMIN_FETCH_DOCTORS_SUCCESS, data};
};
const fetchDoctorsFailure = error => ({type: ADMIN_FETCH_DOCTORS_FAILURE, error});

const fetchDoctorIdSuccess = data => {
    return {type: ADMIN_FETCH_DOCTOR_ID_SUCCESS, data};
};
const fetchDoctorIdFailure = error => ({type: ADMIN_FETCH_DOCTOR_ID_FAILURE, error});

const fetchClinicIdSuccess = data => {
    return {type: ADMIN_FETCH_CLINIC_ID_SUCCESS, data};
};
const fetchClinicIdFailure = error => ({type: ADMIN_FETCH_CLINIC_ID_FAILURE, error});

const fetchClinicsSuccess = data => {
    return {type: ADMIN_FETCH_CLINICS_SUCCESS, data};
};

const fetchClinicsFailure = error => ({type: ADMIN_FETCH_CLINICS_FAILURE, error});


const toggleApprovedSuccess = () => ({type: ADMIN_TOGGLE_APPROVED_SUCCESS});

const toggleApprovedFailure = error => ({type: ADMIN_TOGGLE_APPROVED_FAILURE, error});

const deleteSuccess = () => ({type: ADMIN_DELETE_SUCCESS});

const deleteFailure = error => ({type: ADMIN_DELETE_FAILURE, error});

const fetchDoctorCategoriesSuccess = data => {
    return {type: ADMIN_FETCH_DOCTOR_CATEGORIES_SUCCESS, data};
};
const fetchDoctorCategoriesFailure = error => ({type: ADMIN_FETCH_DOCTOR_CATEGORIES_FAILURE, error});

const fetchDoctorCategorySuccess = data => {
    return {type: ADMIN_FETCH_DOCTOR_CATEGORY_SUCCESS, data};
};
const fetchDoctorCategoryFailure = error => ({type: ADMIN_FETCH_DOCTOR_CATEGORY_FAILURE, error});


const addDataSuccess = data => {
    return {type: ADMIN_ADD_SUCCESS, data};
};
const addDataFailure = error => ({type: ADMIN_ADD_FAILURE, error});

const fetchAdminNotificationSuccess = data => {
    return {type: FETCH_ADMIN_NOTIFICATIONS_SUCCESS, data};
};

const fetchAdminNotificationsFailure = error => ({type: FETCH_ADMIN_NOTIFICATIONS_FAILURE, error});

export const adminToggleWebsocket = () => {
    return {type: ADMIN_TOGGLE_WEBSOCKET}
};

export const fetchClinicsAdmin = () => {
    return dispatch => {
        return axios.get('/admin/clinics').then(
            response => dispatch(fetchClinicsSuccess(response.data)),
            error => dispatch(fetchClinicsFailure(error))
        );
    };
};

export const fetchDoctorsAdmin = () => {
    return dispatch => {
        return axios.get('/admin/doctors').then(
            response => dispatch(fetchDoctorsSuccess(response.data)),
            error => dispatch(fetchDoctorsFailure(error))
        );
    };
};

export const fetchDoctorCategoriesAdmin = () => {
    return dispatch => {
        return axios.get('/admin/doctor_category').then(
            response => dispatch(fetchDoctorCategoriesSuccess(response.data)),
            error => dispatch(fetchDoctorCategoriesFailure(error))
        );
    };
};

export const fetchDoctorCategoryAdmin = id => {
    return dispatch => {
        return axios.get('/admin/doctor_category/' + id).then(
            response => dispatch(fetchDoctorCategorySuccess(response.data)),
            error => dispatch(fetchDoctorCategoryFailure(error))
        );
    };
};

export const fetchClinicIdAdmin = (id) => {
    return dispatch => {
        return axios.get('/admin/clinic/' + id).then(
            response => dispatch(fetchClinicIdSuccess(response.data)),
            error => dispatch(fetchClinicIdFailure(error))
        );
    };
};

export const fetchDoctorIdAdmin = (id) => {
    return dispatch => {
        return axios.get('/admin/doctor/' + id).then(
            response => dispatch(fetchDoctorIdSuccess(response.data)),
            error => dispatch(fetchDoctorIdFailure(error))
        );
    };
};

export const approveProfile = (route, id) => {
    return (dispatch, getState) => {
        let state = getState();
        if (state.admin.clinicId.approved === "approved" || state.admin.doctorId.approved === "approved") {
            NotificationManager.warning('Аккаунт уже подтвержден');
        } else {
            let data = {approved: "approved"};
            return axios.put('/admin/' + route + '/' + id, data).then(
                () => {
                    dispatch(toggleApprovedSuccess());
                    NotificationManager.success('Подтверждено успешно');
                    dispatch(push('/admin/' + route + 's'));
                },
                error => {
                    dispatch(toggleApprovedFailure(error.response.data));
                }
            )
        }
    }
};

export const declineProfile = (route, id) => {
    return (dispatch, getState) => {
        let state = getState();
        if (state.admin.clinicId.approved === "declined" || state.admin.doctorId.approved === "declined") {
            NotificationManager.warning('Аккаунт уже отклонен');
        } else {
            let data = {approved: "declined"};
            return axios.put('/admin/' + route + '/' + id, data).then(
                () => {
                    dispatch(toggleApprovedSuccess());
                    NotificationManager.success('Подтверждение отклонено');
                    dispatch(push('/admin/' + route + 's'));
                },
                error => {
                    dispatch(deleteFailure(error.response.data));
                }
            )
        }
    }
};

export const addNewDoctorCategory = data => {
    return async dispatch => {
        try {
           const response = await axios.post('/admin/doctor_category', data);
            dispatch(addDataSuccess());
            NotificationManager.success(response.data.message);
            dispatch(push('/admin/doctor_category'))
        } catch (e) {
            dispatch(addDataFailure(e));
        }
    }
};

export const editDoctorCategory = (data, id) => {
    return async dispatch => {
        try {
            const response = await axios.put('/admin/doctor_category/' + id, data);
            dispatch(addDataSuccess());
            NotificationManager.success(response.data.message);
            dispatch(push('/admin/doctor_category'))
        } catch (e) {
            dispatch(addDataFailure(e));
        }
    }
};

export const deleteDoctorCategory = id => {
    return dispatch => {
        return axios.delete('/admin/doctor_category/' + id).then(
            response => {
                dispatch(deleteSuccess());
                NotificationManager.success(response.data.message);
                dispatch(fetchDoctorCategoriesAdmin())
            },
            error => {
                dispatch(deleteFailure(error));
                NotificationManager.error("Удаление невозможно, в данной категории есть врачи");
            }
        )
    }
};

export const fetchAdminNotifications = () => {
    return dispatch => {
            return axios.get('/admin/notifications').then(
                response => dispatch(fetchAdminNotificationSuccess(response.data)),
                error => dispatch(fetchAdminNotificationsFailure(error))
            );
    };
};

export const changeAdminNotificationStatus = (id) => {
    return dispatch => {
        return axios.put('/admin/notifications/' + id).then(
            () => dispatch(fetchAdminNotifications()),
            error => dispatch(fetchAdminNotificationsFailure(error))
        );
    };
};