import axios from "../../axios";
import {push} from "connected-react-router";
import {NotificationManager} from "react-notifications";

export const FETCH_DATETIME_SUCCESS = 'FETCH_DATETIME_SUCCESS';
export const FETCH_DATETIME_ERROR = 'FETCH_DATETIME_ERROR';

const fetchDateTimeSuccess = data => ({type: FETCH_DATETIME_SUCCESS, data});
const fetchDateTimeError = data => ({type: FETCH_DATETIME_ERROR, data});

export const createBooking = (data) => {
    return dispatch => {
        axios.post('/records', data)
            .then(
                (response) => {
                    NotificationManager.success(response.data.message);
                    dispatch(push('/'));
                },
                error => {
                    dispatch(fetchDateTimeError(error));
                    NotificationManager.error(error.message);
                });
    }
};

export const getTimesOfDay = (id, date) => {
    return dispatch => {
        axios.get('records/' + id + '?datetime=' + date).then(
            response => {
                dispatch(fetchDateTimeSuccess(response.data));
            }
        )
    }
};