import {
    FETCH_CARD_INFO_SUCCESS,
    FETCH_MEDICAL_CARDS_SUCCESS,
    FETCH_PATIENT_ID_SUCCESS,
    FETCH_QUESTIONNAIRE_SUCCESS
} from "../actions/medicalCards";

const initialState = {
    patient: null,
    medicalCards: [],
    cardInfo: null,
    questionnaire: []
};

const medicalCards = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PATIENT_ID_SUCCESS:
            return {
                ...state,
                patient: action.data
            };
        case FETCH_MEDICAL_CARDS_SUCCESS:
            return {
                ...state,
                medicalCards: action.data
            };
        case FETCH_CARD_INFO_SUCCESS:
            return {
                ...state,
                cardInfo: action.data
            };
        case FETCH_QUESTIONNAIRE_SUCCESS:
            return {
                ...state,
                questionnaire: action.data
            }
        default:
            return state;
    }
};

export default medicalCards;
