import {
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER, ADD_PHOTO_SUCCESS, SAVE_WEBSOCKET
} from "../actions/usersActions";

import {
    DELETE_DATA_AFTER_LOGOUT,
    FETCH_CLINIC_PROFILE_SUCCESS,
    FETCH_DOCTOR_PROFILE_SUCCESS,
    FETCH_PATIENT_SUCCESS, FETCH_SHOP_PROFILE_SUCCESS
} from "../actions/actionTypes";


const initialState = {
    registerError: null,
    loginError: null,
    user: null,
    patientProfile: null,
    doctorProfile: null,
    shopProfile: null,
    clinicProfile: null,
    websocket: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SAVE_WEBSOCKET:
            return {
                ...state,
                websocket: action.id
            };
        case REGISTER_USER_SUCCESS:
            return {
                ...state,
                registerError: null,
                user: action.user
            };
        case ADD_PHOTO_SUCCESS:
            return {
                ...state,
                registerError: null,
                user: {...state.user, image: action.image}
            };
        case REGISTER_USER_FAILURE:
            return {
                ...state,
                registerError: action.error
            };
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.user,
                loginError: null,
            };
        case LOGIN_USER_FAILURE:
            return {
                ...state,
                loginError: action.error
            };
        case LOGOUT_USER:
            return {
                ...state,
                user: null
            };
        case FETCH_DOCTOR_PROFILE_SUCCESS:
            return {...state, loading: false, doctorProfile: action.data};

        case FETCH_PATIENT_SUCCESS:
            return {...state, patientProfile: action.data};

        case FETCH_CLINIC_PROFILE_SUCCESS:
            return {...state, clinicProfile: action.data};

        case FETCH_SHOP_PROFILE_SUCCESS:
            return {...state, shopProfile: action.data};

        case DELETE_DATA_AFTER_LOGOUT:
            return {...state, patientProfile: null, doctorProfile: null, shopProfile: null, clinicProfile: null};
        default:
            return state;
    }
};

export default reducer;
