import {
    FETCH_NOTIFICATIONS_SUCCESS,
    FETCH_NOTIFICATIONS_FAILURE, FETCH_ONE_NOTIFICATION_SUCCESS, DELETE_NOTIFICATIONS_AFTER_LOGOUT, TOGGLE_WEBSOCKET,
} from "../actions/notificationsActions";

const initialState = {
    error: null,
    notifications: [],
    oneNotification: '',
    websocketMessage: false
};

const notificationsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NOTIFICATIONS_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case FETCH_NOTIFICATIONS_SUCCESS:
            return {
                ...state,
                notifications: action.data
            };
      
        case FETCH_ONE_NOTIFICATION_SUCCESS:
            return {
                ...state,
                oneNotification: action.data
            };
        case DELETE_NOTIFICATIONS_AFTER_LOGOUT:
            return {
                ...state,
                notifications: []
            };
        case TOGGLE_WEBSOCKET:
            return {
                ...state,
                websocketMessage: !state.websocketMessage
            };
        default:
            return state;
    }
};
export default notificationsReducer;