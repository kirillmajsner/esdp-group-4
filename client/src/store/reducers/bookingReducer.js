import {
    FETCH_DATETIME_ERROR,
    FETCH_DATETIME_SUCCESS
} from "../actions/bookingActions";

const initialState = {
    error: null,
    bookingTime: null

};

const bookingReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DATETIME_SUCCESS:
            return {
                ...state,
                bookingTime: action.data,
                error: null
            };
        case FETCH_DATETIME_ERROR:
            return {
                ...state,
                error: action.data
            };
        default:
            return state;
    }
};
export default bookingReducer;