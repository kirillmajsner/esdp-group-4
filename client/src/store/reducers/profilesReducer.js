import {
    FETCH_CATEGORY_SUCCESS,
    FETCH_CLINICS_LIST_SUCCESS,
    FETCH_DATA_FAILURE,
    FETCH_DATA_REQUEST,
    FETCH_PROFILES_SUCCESS,
    ADD_DATA_FAILURE,
    FETCH_DOCTOR_SUCCESS,
    FETCH_DOCTORS_SUCCESS,
    FETCH_PROFILE_FAILURE,
    ADD_DATA_SUCCESS, SEARCH_DATA,
    FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS,
    FETCH_CLINIC_INFO_SUCCESS, FETCH_PATIENT_FOR_RECORD, FETCH_CLINIC_DOCTORS_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
    profiles: [],
    loading: true,
    error: null,
    doctorCategories: [],
    doctors: [],
    doctorPublic: null,
    clinics: [],
    clinicProfiles: [],
    infoClinic: [],
    patientForRecord: "",
    clinicDoctors: []
};

const profilesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DATA_REQUEST:
            return {...state, loading: true};

        case FETCH_DATA_FAILURE:
            return {...state, loading: false, error: action.error};

        case FETCH_PROFILES_SUCCESS:
            return {...state, loading: false, profiles: action.profiles};

        case FETCH_DOCTORS_SUCCESS:
            return {...state, loading: false, doctors: action.doctors};

        case FETCH_DOCTOR_SUCCESS:
            return {...state, loading: false, doctorPublic: action.doctor};

        case FETCH_PROFILE_FAILURE:
            return {...state, error: action.error};

        case FETCH_CATEGORY_SUCCESS:
            return {...state, doctorCategories: action.data};

        case FETCH_CLINICS_LIST_SUCCESS:
            return {...state, clinics: action.data};

        case ADD_DATA_SUCCESS:
            return {...state, error: null};

        case ADD_DATA_FAILURE:
            return {...state, error: action.error.response.data};

        case SEARCH_DATA:
            return {...state, doctors: action.filterData};

        case FETCH_CLINIC_DOCTOR_PROFILE_SUCCESS:
            return {...state, clinicProfiles: action.data};

        case FETCH_CLINIC_INFO_SUCCESS:
            return {...state, infoClinic: action.data};
        case FETCH_PATIENT_FOR_RECORD:
            return {
                ...state,
                patientForRecord: action.data
            };
        case FETCH_CLINIC_DOCTORS_SUCCESS:
            return {...state, clinicDoctors: action.data};
        default:
            return state
    }
};

export default profilesReducer;
