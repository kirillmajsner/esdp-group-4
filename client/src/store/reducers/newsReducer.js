import {FETCH_NEWS_SUCCESS, FETCH_ONE_NEWS} from "../actions/newsActions";

const initialState = {
    news: [],
    onePost: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWS_SUCCESS:
            return {
                ...state,
                news: action.news
            };
        case FETCH_ONE_NEWS:
            return {...state, onePost: action.post};
        default:
            return state;
    }
};

export default reducer;
