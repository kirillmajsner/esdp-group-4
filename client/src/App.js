import React, {Component} from 'react';
import {Alert, Container,} from "reactstrap";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer, NotificationManager} from "react-notifications";

import {logoutUser, saveWebsocket} from "./store/actions/usersActions";

import Routes from "./Routes";
import Toolbar from "./Components/UI/Toolbar/Toolbar";
import Footer from "./Components/Footer/Footer";
import AdminPanel from "./containers/AdminContainers/AdminPanel";
import AdminToolbar from "./Components/UI/Toolbar/AdminToolbar";
import {toggleWebsocket} from "./store/actions/notificationsActions";
import {adminToggleWebsocket} from "./store/actions/adminActions";
import config from "./constants";

class App extends Component {

    state = {
        websocket: null
    };

    makeConnection = async (token) => {
        await this.setState({websocket: new WebSocket(config.wsURL + '/notification?token=' + token)});

        this.state.websocket.onmessage = event => {
            const decodedMessage = JSON.parse(event.data);
            if(this.props.user.role === "admin"){
                this.props.adminToggleWebsocket();
            } else {
                this.props.toggleWebsocket();
            }
            NotificationManager.info(decodedMessage.data, "Внимание", 10000);
        };

        this.props.saveWebsocket(this.state.websocket);
    };

    componentDidMount() {
        if (this.props.user) {
            const token = this.props.user.token;
            this.makeConnection(token).then(
                () => {
                    ///
                }
            )
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.user !== this.props.user) {
            if (this.props.user) {
                const token = this.props.user.token;
                this.makeConnection(token).then(
                    () => {
                        ///
                    }
                )
            } else {
                this.state.websocket.close();
            }
        }
    }

    render() {
        return (
            this.props.user && this.props.user.role === "admin" ?

                <div className="wrapper">
                    <div className="content">
                        <NotificationContainer/>
                        <header>
                            <AdminToolbar
                                logout={this.props.logoutUser}
                                websocketMessage={this.props.adminWebsocketMessage}
                                toggleWebsocket={this.props.adminToggleWebsocket}
                            />

                        </header>
                        <AdminPanel user={this.props.user}/>
                    </div>

                    <footer>
                        <Footer/>
                    </footer>

                </div>
                :
                <div className="wrapper">
                    <div className="content">
                        <NotificationContainer/>
                        <header>
                            <Toolbar
                                user={this.props.user}
                                logout={this.props.logoutUser}
                                websocketMessage={this.props.websocketMessage}
                                toggleWebsocket={this.props.toggleWebsocket}
                            />
                        </header>
                        {this.props.user && this.props.user.activate === false ?
                            <Alert color='danger'>Активируйте аккаунт!!!</Alert> : null}
                        <Container className="py-4">
                            <Routes
                                user={this.props.user}
                            />
                        </Container>
                    </div>

                    <footer>
                        <Footer/>
                    </footer>

                </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    websocket: state.users.websocket,
    websocketMessage: state.notification.websocketMessage,
    adminWebsocketMessage: state.admin.adminWebsocketMessage
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => {
        dispatch(logoutUser())
    },
    saveWebsocket: id => dispatch(saveWebsocket(id)),
    toggleWebsocket: () => dispatch(toggleWebsocket()),
    adminToggleWebsocket: () => dispatch(adminToggleWebsocket())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps,)(App));
