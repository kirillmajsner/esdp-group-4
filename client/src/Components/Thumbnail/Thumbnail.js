import React from 'react';
import config from "../../constants";
import Image from "../../assets/images/noimage.png";

const Thumbnail = props => {

    let image = Image;

    if (props.image) {
        image = config.apiURL + '/uploads/' + props.image;
    }

    return <img src={image} className="doctor-img" alt="Img"/>
};

export default Thumbnail;
