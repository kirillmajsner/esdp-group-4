import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";

import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";


class ShopProfilePage extends Component {

    render() {
        const shop = this.props.shopProfile;
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Fragment>
                        {this.props.shopProfile ?
                            <div>
                                <h1>{shop.title}</h1>

                                <p>Описание: {shop.description}</p>
                                <p>Адрес: {shop.address}</p>
                                <p>Номер телефона: {shop.phone}</p>

                                <Link to='/cabinet/profile'>
                                    <Button id="editShopProfile" color="primary">
                                        Редактировать данные
                                    </Button>
                                </Link>
                            </div>
                            : <p>Заполните анкету</p>}
                    </Fragment>
                </Col>
            </Row>

        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.profiles.error,
    shopProfile: state.users.shopProfile
});


export default connect(mapStateToProps)(ShopProfilePage);