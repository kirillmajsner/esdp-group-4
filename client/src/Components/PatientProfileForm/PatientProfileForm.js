import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Row} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import DatePicker from "react-datepicker/es";
import inputMask from "react-input-mask";

import {path} from "../../constants";
import {Link} from "react-router-dom";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";


class PatientProfileForm extends Component {

    constructor(props) {
        super(props);
        if (props.data) {
            this.state = {...props.data};
        } else {
            this.state = {
                name: '',
                surname: '',
                thirdname: '',
                gender: '',
                passportId: '',
                passportNumber: '',
                phone: '',
                workAddress: '',
                residence: '',
                dateOfBirth: ''
            };
        }
    }

    dateFormat = date => {
        let d = new Date(date);
        return d.toLocaleDateString('ru-GB');
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };


    dateChangeHandler = date => {
        this.setState({
            dateOfBirth: date
        });
    };

    submitFormHandler = event => {
        event.preventDefault();


        this.props.onSubmit(path.patientProfile, {...this.state});
    };

    render() {

        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Form onSubmit={this.submitFormHandler}>
                        <Row>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="name"
                                    title="Введите ваше имя:"
                                    type="text"
                                    value={this.state.name}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('name')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="surname"
                                    title="Введите вашу фамилию:"
                                    type="text"
                                    value={this.state.surname}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('surname')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="thirdname"
                                    title="Введите ваше отчество:"
                                    type="text"
                                    value={this.state.thirdname}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('thirdname')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    type="select"
                                    title="Выберите пол:"
                                    propertyName="gender"
                                    value={this.state.gender}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('gender')}
                                >
                                    <option value=""/>
                                    <option value="male">Мужской</option>
                                    <option value="female">Женский</option>
                                </FormElement>
                            </Col>
                            <Col xs="12" md="6">
                                <FormGroup>
                                    {this.props.data ?
                                        <FormElement
                                            propertyName="dateOfBirth"
                                            title="Дата рождения"
                                            type="text"
                                            value={this.dateFormat(this.state.dateOfBirth)}
                                            readOnly
                                            error={this.getFieldHasError('dateOfBirth')}
                                        />
                                        : <Fragment>
                                            <label>Дата рождения:</label>
                                            <DatePicker
                                                onChange={this.dateChangeHandler}
                                                selected={this.state.dateOfBirth}
                                                className="form-control"
                                            />
                                        </Fragment>}
                                </FormGroup>
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="passportId"
                                    title="Введите id паспорта:"
                                    type="text"
                                    value={this.state.passportId}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('passportId')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="passportNumber"
                                    title="Введите номер паспорта:"
                                    type="text"
                                    value={this.state.passportNumber}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('passportNumber')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="phone"
                                    title="Номер телефона:"
                                    mask="+\9\96 (999) 99-99-99"
                                    tag={inputMask}
                                    maskChar="_"
                                    type="phone"
                                    value={this.state.phone}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('phone')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="workAddress"
                                    title="Рабочий адрес:"
                                    type="text"
                                    value={this.state.workAddress}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('workAddress')}
                                />
                            </Col>
                            <Col xs="12" md="6">
                                <FormElement
                                    propertyName="residence"
                                    title="Адрес проживания:"
                                    type="text"
                                    value={this.state.residence}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('residence')}
                                />
                            </Col>
                        </Row>
                        <Link to='/cabinet'>
                            <Button className="mr-3" color="secondary">
                                Отмена
                            </Button>
                        </Link>
                        <Button id="savePatientProfile" type="submit" color="primary">Сохранить</Button>
                    </Form>
                </Col>
            </Row>

        );
    }
}


export default PatientProfileForm;
