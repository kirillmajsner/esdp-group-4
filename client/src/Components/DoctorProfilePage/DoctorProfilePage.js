import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";


class DoctorProfilePage extends Component {

    dateFormat = date => {
        let d = new Date(date);
        return d.toLocaleDateString('ru-GB');
    };

    render() {
        const doctor = this.props.doctorProfile;
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Fragment>
                        {this.props.doctorProfile ?
                            <div>
                                <h1>{doctor.surname} {doctor.name} {doctor.thirdname}</h1>

                                <h4>Личные данные</h4>
                                <p>Дата рождения: {this.dateFormat(doctor.dateOfBirth)}</p>
                                <p>Паспортные данные: {doctor.passportId} {doctor.passportNumber}</p>
                                <p>Пол: {doctor.gender === "male" ? "мужской" : "женский"}</p>
                                <p>Номер телефона: {doctor.phone}</p>

                                <h4>Профессиональные данные</h4>
                                <p>Специальность: {doctor.speciality}</p>
                                <p>Категория: {doctor.doctorCategory.doctorCategory}</p>
                                <p>Лицензия: {doctor.license}</p>
                                <p>Диплом: {doctor.diploma}</p>
                                <p>Стаж работы: {doctor.yearsOfWork}</p>
                                <p>Место работы: {doctor.workAddress}</p>
                                <p>Клиника: {doctor.clinic.title}</p>
                                <p>Режим работы: {doctor.schedule}</p>
                                <p>Стоимость: {doctor.price}</p>
                                <Link to='/cabinet/profile'>
                                    <Button id="editDoctorsProfile" color="primary">
                                        Редактировать данные
                                    </Button>
                                </Link>
                            </div>
                            : <p>Заполните анкету</p>}
                    </Fragment>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.profiles.error,
    doctorProfile: state.users.doctorProfile
});


export default connect(mapStateToProps)(DoctorProfilePage);