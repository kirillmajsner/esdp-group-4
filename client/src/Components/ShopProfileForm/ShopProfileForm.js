import React, {Component} from 'react';
import {Button, Col, Form, Row} from "reactstrap";
import inputMask from "react-input-mask";
import {path} from '../../constants';
import {Link} from "react-router-dom";

import FormElement from "../UI/Form/FormElement";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";

class ShopProfileForm extends Component {

    constructor(props) {
        super(props);
        if (props.data) {
            this.state = {...props.data};
        } else {
            this.state = {
                title: '',
                description: '',
                address: '',
                phone: ''
            };
        }
    }

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };


    getFieldHasError = fieldName => {
        return (
            this.props.error &&
            this.props.error.errors &&
            this.props.error.errors[fieldName] &&
            this.props.error.errors[fieldName].message
        );
    };

    submitFormHandler = event => {
        event.preventDefault();


        this.props.onSubmit(path.shopProfile, {...this.state});
    };

    render() {
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Form onSubmit={this.submitFormHandler}>
                        <Row>
                            <Col xs='12' md='6'>
                                <FormElement
                                    propertyName="title"
                                    title="Введите название магазина:"
                                    type="text"
                                    value={this.state.title}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('title')}
                                />
                            </Col>
                            <Col xs='12' md='6'>
                                <FormElement
                                    propertyName="description"
                                    title="Введите описание магазина:"
                                    type="textarea"
                                    value={this.state.description}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('description')}
                                />
                            </Col>
                            <Col xs='12' md='6'>
                                <FormElement
                                    propertyName="phone"
                                    title="Введите номер телефона:"
                                    mask="+\9\96 (999) 99-99-99"
                                    tag={inputMask}
                                    maskChar="_"
                                    type="text"
                                    value={this.state.phone}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('phone')}
                                />
                            </Col>
                            <Col xs='12' md='6'>
                                <FormElement
                                    propertyName="address"
                                    title="Укажите адрес магазина:"
                                    type="text"
                                    value={this.state.address}
                                    onChange={this.inputChangeHandler}
                                    error={this.getFieldHasError('address')}
                                />
                            </Col>
                        </Row>
                        <Link to='/cabinet'>
                            <Button className="mr-3" color="secondary">
                                Отмена
                            </Button>
                        </Link>
                        <Button id="saveShopProfile" type="submit" color="primary">Сохранить</Button>
                    </Form>
                </Col>
            </Row>

        );
    }
}


export default ShopProfileForm;

