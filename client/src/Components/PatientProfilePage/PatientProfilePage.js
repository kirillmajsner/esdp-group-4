import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";


class PatientProfilePage extends Component {

    dateFormat = date => {
        let d = new Date(date);
        return d.toLocaleDateString('ru-GB');
    };

    render() {
        const patient = this.props.patientProfile;
        return (
            <Fragment>
                <Row>
                    <Col xs="12" md="3">
                        <UserCabinetMenu/>
                    </Col>
                    <Col xs="12" md="9">
                        {this.props.patientProfile ?
                            <div>
                                <h1>{patient.surname} {patient.name} {patient.thirdname}</h1>

                                <p>Дата рождения: {this.dateFormat(patient.dateOfBirth)}</p>
                                <p>Паспортные данные: {patient.passportId} {patient.passportNumber}</p>
                                <p>Место проживания: {patient.residence}</p>
                                <p>Место работы: {patient.workAddress}</p>
                                <p>Пол: {patient.gender === "male" ? "мужской" : "женский"}</p>
                                <p>Номер телефона: {patient.phone}</p>

                                <Link to='/cabinet/profile'>
                                    <Button id="editPatientProfile" color="primary">
                                        Редактировать данные
                                    </Button>
                                </Link>
                            </div>
                            : <p>Заполните анкету</p>}
                    </Col>
                </Row>

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.profiles.error,
    patientProfile: state.users.patientProfile
});


export default connect(mapStateToProps)(PatientProfilePage);