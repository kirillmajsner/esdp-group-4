import React from 'react';
import {Card, CardBody, CardTitle} from "reactstrap";



const ClinicListItems = props => {
    return (
        <div>
            <Card style={{marginTop: '10px'}}>
                <CardBody>
                    {props.children}
                    <CardTitle>
                        Название: {props.title}
                    </CardTitle>
                    <p>Описание: {props.description}</p>
                    <p>Адрес клиники: {props.address}</p>
                    <p>Номер телефона: {props.phone}</p>
                </CardBody>
            </Card>
        </div>
    );
};

export default ClinicListItems;