import React, {Fragment} from 'react';
import {
    Button,
    Card,
    CardBody, CardFooter, CardImg, CardSubtitle, CardText, CardTitle,
    Col,
    ListGroup,
    ListGroupItem,
    NavLink,
    Row
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';


const DoctorPage = () => {
    return (
        <Fragment>
            <Row>
                <Col sm="3">
                    <h2>Катергории врачей</h2>
                    <NavLink tag={RouterNavLink}
                             to={'/category/'} exact
                    >
                        <ListGroup>
                            <ListGroupItem>Аллергология и иммунология</ListGroupItem>
                            <ListGroupItem>Акушерство и гинекология</ListGroupItem>
                            <ListGroupItem>Aллерголог</ListGroupItem>
                            <ListGroupItem>Aндролог</ListGroupItem>
                            <ListGroupItem>анестезиолог-реаниматолог</ListGroupItem>
                            <ListGroupItem>аритмолог</ListGroupItem>
                            <ListGroupItem>рефлексотерапевт</ListGroupItem>
                            <ListGroupItem>невролог</ListGroupItem>
                            <ListGroupItem>нейрохирург</ListGroupItem>
                        </ListGroup>
                    </NavLink>
                </Col>
                <Col sm="9">
                    <h2>Список врачей</h2>
                    <Card>
                        <CardBody>
                            <CardImg className='float-left' top style={{width: '150px', marginRight: '15px'}}
                                     src='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEhAQDxAPEBAPEA8QDxAPDw8PDw8QFRUXFhUWFRUYHSggGBolGxUVITIhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFy0fHSItLS0rKystKy0tLS0tLS0tLS0tLSstLS0tLS0vLSsrLS0tLSsrLS0tLS0tLS0tLSs3Lf/AABEIAMABBwMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAADAAECBAUGBwj/xAA8EAACAgECAwUFBQcEAgMAAAABAgADEQQhBRIxBkFRYXEHEyKBkTJCobHRFCNSYsHh8CRygvGiwjNzkv/EABkBAAMBAQEAAAAAAAAAAAAAAAECAwAEBf/EACMRAAICAgICAgMBAAAAAAAAAAABAhEDIRIxBEEiURMyYfD/2gAMAwEAAhEDEQA/APbgI+IhHhFSFI2dJORfpMgy6MXUr8UHWmTLGpG8jSN5azno06KgBC8savpJSLey8UqFywOr1VdStZa6VooyzuwVR8zKfaLjdWjofUXH4V2VRjmdj0UZ7587dp+1Oo4haXtcitSTXWP/AI6l8h4+Z3MwaPYOI+1LQ1nlrFt/mihVPpzbn6SkvtYrPTR2Y/8AtUH6YniS3eBwvex+00spf0x8vH6TBo9+4V7QdLaQHWykn+MBh9VnUabVV2ANW6Op70YMPwnzXo9cBhXOPHpj+0LrtWytW1VttdoYFDWzKfQkflNRmfS0U8t7Ee0J8pRxBshvhTUYAIPhYBt8/wDueoq2cEd8Bh4oopjCle8SxA3CNHsTItGY8hDOsjyzpOKgcaE5Y2JjUQxHksR8TGohHksRYgNRGNJ4kTMahpAyRkCYQMiwjRmMUIp0QkoPmjhpyUegpInIt0izGYwDN6M3UjeRqG8JqBvIV9ZYgadfSSkaukjfcqKzuQqopZiegAGSZJ9l10eJe2bjZu1P7MpPutNgYHR7mAJ27yAQJ5vqX5R7vpjBsPfnuUTf7Q6/3uotvH3rLLB/uY7Y+RH4zk7WOc9Tk8vmT1aYIWsknHh9FE0acAEjoOrf5+UyqrAox47nG5Y+A/WaOmVmI5tgBkIOijxMzY0VYenTlyMd5wPX9Z2ul7I221cw+0qkD9PWF7E8A58WsDjom2MeJ9fynqWgpCgADAGwE53kbdI6FjUVs8RSnDtS4KuuzKdtu4iemezPtIT/AKG8ksgJoY/eUfd+Xd6GZPtS4SB7vWVrh62CPjvRthn0OJymm1JUpqKyQ9bK+e8biPGVk5w+j6FilXhmsW6qu5fs2orjyyOktShEaDuhYK6FdiT6KLSMk0jOg5Ro0eNCAWIo8aAwooopjD4kGEkIzTGBGQYSbRoxNgmEUkRFCA0BqR4yQ1E51dQYddQYvBFVM31uk2s2mLTeZcS/IiOA8ZkrDvGSRzHWAdGhU205f2o6opw+3BxzPUhx4FwT+U6FHnE+13UD9iVT0e+v6KGb+kTiPy9HimrIAye/fHr0Ey7aSPiOOZvogl4KzHnYYH2t/E9B9Jo8K4T+0OA5ZU65VS30AizairLY05OjE4doyzDAJJ6YGWPoJ6V2V7EliH1I5UHxCkHdz4uRv8pnaPT16d/9Nq68/wAN1DAn/lidzwHjfP8AC7V8wx9hsg/pOWcmzshBJG9pdOq4VQAF2AAwAJp1iY+s1fu15gVB8WOBMbTcZ1DPvq9EqeAPM0nEaSNftjXzaW4dcKTj03nj3B9SM8jdHVhv5DcfQ/8AjPaOJWrZQ/xI37s5K4Pd4TwLhNueU96WYPyyPxzKR9k30j3z2YagtogrHPurrKx6bN/7Tr5w3s020p/mudvX4VBP4Ts1edSWkzicqk0FgbWkmeVrXjRiTyT0BaNFmIyxzjGNFFCYeMYooDCiiimMKMY8RmMCMjJtIRkIyJjxGKYUyBDVwMPXGCizWJZQSrWZYRoGMmHEksgskJFnQgwnnXteJZdLUvV2uP0UfrPRBOG9pukLLTZ3J71SPIgH/wBcQPoy7PFtTqAi2BPufCH72sbv9AAfwno3BNFXWq7hOchiegbYd/jPK+JW42O3xsx79z/1Pa+zqB9Pp2/ipqP1UGcmZs9Lx0tmTb2dq5haOYlc8mOXAzv4biVeH8Ls97WgsIUsWbB/eKvkR0Hdj9J12r0NYUkonTqVWVuD6cAA4Vd84Gx69SJDk37OjiktE+O8Jsdkr5i1b1kh3IZ0dcbLtjcTO4Z2SQM7M1gZyM8nKuOv2T3dfync6hFavBCseXIU48O6ZnCtMjDPxeY53/LMCbWrBVroNfpwKFp5nU7BHBU2AL8R3IPcpB9Z8/8AAG/eXr/MzKPQ9J772juWjSam1QAVqcA9+T8I39SJ4H2frzqeVfvhsfQysXaZKS2j3vsG4/ZKseLZ9czqkacR7OW/07eAtOP/AMoZ2lZnbD9UebP92EZpXsMKxlewx4kpkliMGjQhMcmiIkpAGSzMYUUjmPmYI8UiTGLQGJxGQ5ouaY1jNIR3MGXhQjHMUGXjRqFsoLWfCGSszXGmEmNMIOSHUGZaKfCGrBl79nkvcQcgqBXWSEd1xGEmyy6Cic32+pJ0jsPuMufQkA/nOppWC4vw4X0W0tsLUZc+BI2PyODA3qgpbs+Ve0ukIPMBsSTPW+wOrFmh0xB+xUtZ8mT4T+U4ntFw+ysvTehD1sVfYjPg6+RG8sezrifuLG07Nmq1socfYs8/I+PiPOcmRNxr6O/E0nfpne9pdaaqw7I7oCOYVgMwXvbHfiD0DkqltauyOMqwUEYmpcvMMHulXSaN6yfcuUBzlc/CCfLpOZV7O6L/AMzZq1XJWXsV1VVLM5XAVQMknyxBcC1Ndwe6nm5HbbmRkOcDfDb79fnD06Q2L+/c2DbKn7Bx5S7TUF5j05jkzP8AgjaVnFe1zW+70BQH4r7qkA8Qp94fwSeX9i6c3K/gf6H+86j2h9o01Vr6epeYUn3avvgswIcr47kD/jL3sy7Ne8tBYfuqBzWHuZz0T88y8Y/FL2zmlPbk+kd/2M4cadNWCMFybCPDm6D6YnSIsIKpJVnbpKjzbbdgmEq3TQZZXuSGIskUFaEDwF4wZFXlqIXQcvJB5Wd4yvNQbLBeLngSY3NBQLD80YvIrGaY1iayOLJWcxKY1Ccgz2Ss9sm0EajCkhZNkDdFCjTxQ6FqR0YxJSr76OLZzcTu5FiIwIePzTUGwV0HJ2GQmYUXdPLEp0mGLxWhkzK7Q9nKdWuLV+LBAYbECeLdpuHPw6z9nNdTEuHXU4/eGkkfCM7L03M9q7Rcfq0dJutyxLBKqkwbL7W2WtB3kn9ZwftD4XbqtMLyF/aaQWcV7qCpBesE9cDIz34z3zcG1YYzXJL0PwTiBK8rnmA2DA5OO7PjN7TEE9ZwnZfV/Aqtj1nV6SzJnmyPXR0lDqBv0nL9rO0PwtVUwU4IYnI+Q/WbVTgDecv2w4wK6yEANj/CuACcnYY84NvRlV2zk+zXDDe19pB5avjttQFzVWW+Iqg36Z6eBntnZ3R0VUVrpSrVMoZbFPMLM/ez3zJ9nfZ79j0gDge/1BN2oPU8zdF9AMD6ynwpDode2kU/6TXBr9Kp+zVb1srXwB3OJ6WPGq/p5mfJJv8Ah2kQMAzEdRI+9j8Tnui0TA2wfvoOy2FIDZR1plVXh9WZSrbeXS0cknstmMhhUXaQZcQDE4sSSxEQBJoInEeuO4gG9FRxJIsZ4WoRiS7HFckK4QCMYB6EFijZimDY6iFVY6rCoIrY6Q6pJcsksniI2OkVXEjCWwLOB5wFEWqfGVuI8VqpRrHYBEUszE4UADJgLHOMnoO6eZ9vtZZqbaeH1kj32LLsd1Kn4R8yM/IR4Q5MEtF7svZZxTXjiV640un500FROynPKbGH8XU+voM9NpLAL9dWxACNVdkkABXTlY79B8EucA4clFaVooVVUbDxmZ2o4R7wX4yDqKLKXx1P3kPyYY/5R4tSlXoEk0kzlddwtaXD0lX09pLVPWQ6YzuoYbbH8MTSrswA0H2IrVqH07urKu+33T3MAdwcwmuUKOXI2zPP8zB+LJS2j0/Ez/khvtEqdRZc3u6VLt39wA8Se6Du4ONK663XEWqjKK66fi5CersTgHA7hOl7M6VaqQTnnuw7nHcfsjPp+cB2s4Z7+k1pZyADIGOuJfx/Hhaczn8jyJNNQOpe8Go2IQVNfMhG4II2InI9uVauvh+o6Pp7qeY+HQNv6Zln2fanm0iadzltPYyHzrB5l/ML8of2lVBtBaRn4MN6dx/OWS4ZOP8ASCfKNnS3JkZG3ftKwIzhgPIjvj8K1Qsoqs/irRvLcSN7jmAklrRRpMIdKO4/1EBbpmHQZ9P0h9O+BLBYCbk0K8aZzup8MH5zObIYTr9TUtikHrjY94M5fULynDbFTg+s6Mc70cGfE47LunbIk7VgdI4Pf0ltd5m9mjtEK1k2WTRYQrFsdLQKpY7rJ1iScQWGtGbYu8NUIrBvCoI9kktjyJkzIwDEDFJGKEBcAkwIyyYk2WSHURrXwPOTErXH4opSKK7MT1iVY+IQQFUivqkJVsdcGebcK+LimusZS/uK6K1GegIHf6gz0u2zHWeU0dpadPxLXWoGvqs92pNRQ7qBk7kZGebfyl8PTJz7R6pw+3nGQMDG/fiHsrGdxkEY26iY3ZntHpdSp9w45+r1MOS1e7JU93mNp0GJGSaf0UW0ebcR4MdHc1yEgO5esjI2JyV+Xh4QljLqhl/3VuPthT7ph3gj7p8x9J3uu0SXIa7RlTuD3q3cRMG/h3ISnL9kYz3EHoROjlDLGprZH54pXBmv79eVQgyMADHTHlKXEb/dqSepG3gJHSqyYBzjbI/rAcQc2WLWnVyB6Z74vGmHlaDdh9DypbeQR75wF/2rnf6kzS7U182luBO3u7D6gDv/AM7praSgIqovRQAPlKvHkzp7l8a2H4SDlynZeMajRhdhNQX4fpck5VAhH+3I/pNq74yFGTg7nwnOezQ54egzultoH16fjOurTA/OHJqTBHpEK1HKR6xq26E9wx6SZOTCgRByKnJJHTGJg9olwa3xschvUdPwnRZgtZpxYjIe8beR7iJoPi7JZoc4NHJ6W8E+W+fPymtpm88junO1Aq2D1BwfUTf07ZAnVNHnYZ3ph8yZMG0mBJl0JDJsYJRvDYgYUUbW3k63gtUINHlEtEG6ZaZ5HngsxwJg2TLRSOI0ILNcLJgRogZA60iUo2NvLlrbGUrIEOiUfEgO76fpJE4ExRHE+0LibkVaKlitmqJDsPtLUMAgHuLEgZ7hmXOF9naNLUi1YGAC22Wdu9jMDiDe943SncKQR9HJ/pPRk0QTzzuZeT4xSROK5Ns867V8NC412jHJqNOediqlPeJ94MuBn+s7rs1xZdVp6r1294u65yVYbEH0MnrdGCrcoG4OR3EeYnPezzhF+lpdL1Cj3rMnKwYgHbu9B9YG1KG+0aNqR2RkbauYea9PMR/Xp4iEHlIXRVqzHvMq9m9OWustPRMqvqev4fnC8fb3ZDfdc49G8P8APOa3B9L7usDvJLN/uO5/zyl5SqF/ZGMflX0aIgdcMowPepEIzYlS5zkTmSOhnK+zH4adTU3WnV2qQeoyZ2DGcd2Qfl1/F6u739dgH+5c/wBZ2arK5f2sSHQ6CTiikhxm6R0MiTGpPWYxzfG9Ly2kjo/xfPvk9I00uP1ZRW/hOPkf74mPp33nVB8oHlZY8MrNUDIkwJCk7Qsmy6IY3k40lMEoawSiGmlq1mY43lYdHNl0yzWYYCAplgTM0RjFEYphi/76IXykgMs11GTaSLKTYSyzI+cHYY7jBAkWHdEZePQy/wDUJiV6m+6flD1nb02gHOSfswx4kus5wqV1gKN+Z2PMDv3AA/PPdOzDZ6/WV7k7x1EnWYZScuwRVErAceMFpTjOBt0MtAZEr6Yfa36NFG9llRnpAupG6nB8D9k/KGH0kLztAMVGUW4V12BBIO4yDkfjNRBgSpoU2LHv6fKWyZmzIZzKmoOAT4Sy0r6jcGZGZx/ZW3PFeJeLJSfmEr/Wd4JzPCE0leoscPUNRdhWJf4nYYHLgnH3RtOmWPkdsWCpEhETGzM7RcQF3Nygjksetg2xDKcdO7x9CJOhi9/FI6c9Ygdj842l6esJiWsTmrcfyn8N5zaLvOqnNsuGI8DiVxPtHF5a2mXKOkPAUGHJhYIvQ4ksSKGTijoBeJnWpvNO2UrRHiyOREalhsSFcLGFS0DxFHMeYxeSmHVI6wgnO5M7FFGdrPtekG7dD3GLUt8RMid19IR0Bt+Eg90HrOICkF2+z1MMCGXB6iZXFKBdVZQ2x5cr5j/v84yVmNjQ6lbqq7k5uS5FdeYcrcrDO47oVPz6+sx+w1jnQ6cWkl6092SRgnkJUZ88Caz9fxED7oxarOxgdL98/wAxjJZnMlRsPUn/AD8IoxYAgdZ0MDqeJ01FBbbXWbG5aw7qpdvBQesNrD8Dek1DFmpcKo/lEZpIHYegkTFCQsaZWqdnW5Eblcoy1t/CxXY/WariZer0bZLIcE/SMhWZ2ufT2JSvKQ1VZrGmVD7zJ2KhehG32ht35nScNR1qqW45sWtA5znLcozv65lPglTIXLqOZ9ywIOfn1mnmB/RkvZMTNQKttgAxzFXbzOMZ/wDGaOZnaoYt5v4q+X5g/wB5kFhi2EHicQ9QwBKxOSB3CWczMwVZgcUr5bD/ADYYfP8AvN5TMvj1eyP4ZUxsb+Rz+VG8d/RU09kthpk1Wby/U0tJHFjnZarMLAVywJNnTEDbKV8u3SjcY0SeQasw0rVmHzHZKI8UgzRpg2XKtcD3y0NQCD6Tj6yR0Jl3Rahiyrnqd/SCWNDRzuzVtOcGQR4ItnmXvG4gE1PcdjJ0dqYUvyv5GUOKtyWVOOnMVPmGH9pav3GR3SrxL46/Mbj1EePZmY9/bmjR3V6JqrXZwrc1ZrCg2PgDDEeIyfOdbbqkIzzAeZyBn5wGj0iFay9dbMEXdkViO/qRLLkeR+UWVWBdFNeIrnAO5OC52rXHUljsfQQ51XN8NKl/Pdax5lj1+WZZpAyAfCFIijpaOU13Yiu/UpqtRbYWUKOSs8i5BPeckAg4OJ1eqPwH0iJgdW2FMDbYejQrOy+g/KImQR8KnmFH4Rc+dxEHHJkYxMWYRQiQggkkuaYIWZ/FE/8AjP8AC34bfpLytKPFX+EesMexZulZLTNkyzzTM0Fn9ZeRszNBTLKmV+JV81TjwHMPlvCgyYgWnZpLkmjkDL9D7SvqKuVmXwJEnQZ1vaPGh8WaNZlkGVKWhueRaOyL0NcZQuMs3PKNzxoonkZAWQ3vJRL7yXvJSjnUiw1sUotbFDRuZ//Z'
                                     alt="Card image cap"/>
                            <CardTitle>
                                <NavLink tag={RouterNavLink} to={'/pofile/'} exact>
                                    Завгородняя Евгения
                                </NavLink></CardTitle>
                            <CardSubtitle>Акушер</CardSubtitle>
                            <CardText><strong>Прием от 1000 сом</strong></CardText>
                            <CardText>пр. Суворовский, д. 60 Клиника «Евромед Инвитро» (Euromed In Vitro)
                                ☎ (812) 317-60-39</CardText>
                        </CardBody>
                        <CardFooter>
                            <Button color='success' style={{marginRight: '5px'}}>Запиаться на прием</Button>
                            <Button color='success'>Расписание</Button>
                        </CardFooter>
                    </Card>
                </Col>
            </Row>
        </Fragment>
    );
};


export default DoctorPage;