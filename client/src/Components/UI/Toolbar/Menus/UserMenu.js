import React from 'react';
import {Badge, DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {Link, NavLink as RouterNavLink} from 'react-router-dom';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUser} from "@fortawesome/free-solid-svg-icons";
import {faEnvelope} from "@fortawesome/free-solid-svg-icons";

const UserMenu = (props) => (
    <UncontrolledDropdown nav inNavbar>
        <DropdownToggle nav caret>
            <FontAwesomeIcon className="mr-3" icon={faUser}/> Профиль
        </DropdownToggle>
      {props.websocketMessage ? <Link className="Notification" onClick={props.toggleWebsocket} to="/notifications"><Badge color="danger" pill> <FontAwesomeIcon
                icon={faEnvelope}/> </Badge></Link>
            : null}
        <DropdownMenu right>
            <DropdownItem tag={RouterNavLink} to="/cabinet">Личный кабинет</DropdownItem>
            <DropdownItem onClick={props.logout}>Выйти</DropdownItem>
        </DropdownMenu>
    </UncontrolledDropdown>
);

export default UserMenu;
