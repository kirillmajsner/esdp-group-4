import React, {Component, Fragment} from 'react';
import {Badge, Button, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import config from "../../constants";
import {connect} from "react-redux";

import imageNotAvailable from "../../assets/images/noimage.png";
import UserImageAddForm from "../UserPhotoAddForm/UserPhotoAddForm";
import {fetchNotifications} from "../../store/actions/notificationsActions";

import './UserCabinetMenu.css';

class UserCabinetMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            id: "",
            number: ""
        };

        this.toggle = this.toggle.bind(this);
    }


    componentDidMount() {
        if (this.props.doctor) {
            this.props.fetchNotifications(this.props.doctor._id, "doctor").then(
                result => {
                    let notifications = result.data.filter(function (notification) {
                        return notification.doctorStatus === "new"
                    });
                    this.setState({number: notifications.length});
                }
            )
        }
        if (this.props.patient) {
            this.props.fetchNotifications(this.props.patient._id, "patient").then(
                result => {
                    let notifications = result.data.filter(function (notification) {
                        return notification.patientStatus === "new"
                    });
                    this.setState({number: notifications.length});
                }
            )
        }

    }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    };

    render() {
        let image = imageNotAvailable;
        let data = "Добавить фото";
        if (this.props.user.image) {
            image = `${config.apiURL}/uploads/${this.props.user.image}`;
            data = "Изменить фото";
        }
        let records;
        let profile;
        let notifications;
        let patientList;
        let medicalCardList;
        switch (this.props.user.profile.codeName) {
            case 'doctor':
                records = <ListGroupItem tag={RouterNavLink} to='/records/doctor'>Записи</ListGroupItem>;
                profile = <ListGroupItem tag={RouterNavLink} to='/cabinet/doctor_profile'>Общие данные</ListGroupItem>;
                notifications = <ListGroupItem tag={RouterNavLink} to='/notifications'>Уведомления <Badge color="danger"
                                                                                                          pill>{this.state.number === 0 ? "" : this.state.number}</Badge></ListGroupItem>;
                medicalCardList = null;
                break;
            case 'patient':
                records = <ListGroupItem tag={RouterNavLink} to='/records/patient'>Мои записи</ListGroupItem>;
                profile = <ListGroupItem tag={RouterNavLink} to='/cabinet/patient_profile'>Общие данные</ListGroupItem>;
                notifications = <ListGroupItem tag={RouterNavLink} to='/notifications'>Уведомления <Badge color="danger"
                                                                                                          pill>{this.state.number === 0 ? "" : this.state.number}</Badge></ListGroupItem>;
                medicalCardList =
                    <ListGroupItem tag={RouterNavLink} to='/cabinet/cards_list'>Истории лечения</ListGroupItem>;
                break;
            case 'shop':
                records = null;
                profile = <ListGroupItem tag={RouterNavLink} to='/cabinet/shop_profile'>Общие данные</ListGroupItem>;
                notifications = null;
                break;
            case 'clinic':
                records = null;
                notifications = null;
                profile = <ListGroupItem tag={RouterNavLink} to='/cabinet/clinic_profile'>Общие данные</ListGroupItem>;
                medicalCardList = null;
                break;
            default:
                records = null;
                notifications = null;
                medicalCardList = null;
        }
        return (
            <Fragment>
                <div className="user-avatar">
                    <img className="ProfilePhoto" src={image} alt=""/>
                    <Button
                        className="my-4"
                        color="primary"
                        onClick={this.toggle}
                    >
                        {data}
                    </Button>
                </div>
                <ListGroup>
                    {profile}
                    {notifications}
                    <ListGroupItem tag={RouterNavLink} to='/cabinet/profile'>Анкета</ListGroupItem>
                    {patientList}
                    {records}
                    {medicalCardList}
                    <ListGroupItem tag={RouterNavLink} to='/cabinet/password'>Сменить пароль</ListGroupItem>
                </ListGroup>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>Обновление фото профиля</ModalHeader>
                    <ModalBody>
                        <UserImageAddForm onClick={this.toggle}/>
                    </ModalBody>
                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    notifications: state.notification.notifications,
    doctor: state.users.doctorProfile,
    patient: state.users.patientProfile
});

const mapDispatchToProps = dispatch => ({
    fetchNotifications: (id, path) => dispatch(fetchNotifications(id, path)),
});


export default connect(mapStateToProps, mapDispatchToProps)(UserCabinetMenu);