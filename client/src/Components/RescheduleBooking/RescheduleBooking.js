import React, {Component} from 'react';
import Calendar from "react-calendar";
import {connect} from "react-redux";
import {Button} from "reactstrap";

import {getTimesOfDay} from "../../store/actions/bookingActions";
import {changeRecord, openModal} from "../../store/actions/recordsActions";

class RescheduleBooking extends Component {

    state = {
        selectedDate: null,
        selectedTime: null,
        nowDate: new Date(),
        isSelectedDate: false,
        isSelectedTime: false,
    };

    componentDidMount() {
        this.onClickedDay(this.state.nowDate)
    }

    formatDate = (date) => {
        const monthNames = [
            "Января", "Февраля", "Марта",
            "Апреля", "Мая", "Июня", "Июля",
            "Августа", "Сентября", "Октября",
            "Ноября", "Декабря"
        ];

        const day = date.getDate();
        const monthIndex = date.getMonth();

        return day + ' ' + monthNames[monthIndex]
    };

    onClickedDay = (value) => {
        const changeValue = value.toLocaleDateString('ru-Ru');
        this.props.getTimesOfDay(this.props.rescheduleId, changeValue);
        this.setState({selectedDate: value, isSelectedDate: true})

    };

    onClickTime = (time) => {
        this.setState({selectedTime: time, isSelectedTime: true})
    };

    changeRecord = () => {
        const {selectedDate, selectedTime} = this.state;
        const data = this.props.data;
        data.date = selectedDate.toLocaleDateString('ru-Ru');
        data.time = selectedTime;
        let date;
        if(this.props.date){
            date = this.props.date;
        }
        this.props.changeRecord(this.props.id, data, date);
        if(this.props.patientProfile){
            this.props.websocket.send(JSON.stringify({type: "RESCHEDULE_RECORD", profile: "patient", id: this.props.id}));
        }
        if(this.props.doctorProfile){
            this.props.websocket.send(JSON.stringify({type: "RESCHEDULE_RECORD", profile: "doctor", id: this.props.id}));
        }

        this.props.openModal();
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        const {bookingTime} = this.props;
        const {selectedDate, isSelectedDate, isSelectedTime, selectedTime} = this.state;

        return (
            <div className={'doctor_record_root'}>
                <div className={'doctor_record_datepicker'}>
                    <Calendar
                        value={this.state.nowDate}
                        onActiveDateChange={() => null}
                        minDate={new Date()}
                        onClickDay={this.onClickedDay}
                    />
                    <h4 className={'doctor_record_root_date_title'}>
                        {
                            selectedDate
                                ?
                                this.formatDate(new Date(selectedDate))
                                :
                                this.formatDate(new Date())
                        }
                        <span className={'doctor_record_root_date_subTitle'}>
                            {selectedTime && selectedTime}
                        </span>
                    </h4>
                    <div>
                        {
                            isSelectedDate &&
                            bookingTime &&
                            bookingTime.map((time, key) => (
                                <button
                                    key={key}
                                    onClick={() => this.onClickTime(time)}
                                    className={'Booking_time_button'}
                                    type='button'
                                >
                                    {time}
                                </button>
                            ))
                        }
                    </div>
                    {isSelectedTime &&
                        <Button className="my-2"
                                color='success'
                                onClick={this.changeRecord}>
                            Отправить
                        </Button>
                    }
                </div>

            </div>


        );
    }
}

const mapStateToProps = state => ({
    bookingTime: state.bookingReducer.bookingTime,
    websocket: state.users.websocket,
    doctorProfile: state.users.doctorProfile,
    patientProfile: state.users.patientProfile
});

const mapDispatchToProps = dispatch => ({
    getTimesOfDay: (id, date) => dispatch(getTimesOfDay(id, date)),
    changeRecord: (id, data, date) => dispatch(changeRecord(id, data, date)),
    openModal: () => dispatch(openModal())
});

export default connect(mapStateToProps, mapDispatchToProps)(RescheduleBooking);