import React, {Component} from 'react';
import {Button, Col, Input, Label, NavLink, Row} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {createMedicalCard, fetchPatientId, fetchQuestionnaire} from "../../store/actions/medicalCards";
import connect from "react-redux/es/connect/connect";
import './MedicalCard.css'

class MedicalCard extends Component {

    state = {
        diagnosis: '',
        pastIllnesses: '',
        presentDisease: '',
        isAllowed: false,
        error: false
    }

    componentDidMount() {
        const id = this.props.match.params.id
        this.props.fetchPatientId(id)
        this.props.fetchQuestionnaire(id)
    }

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    saveInformation = () => {
        if (this.state.diagnosis.trim() !== '') {
            const data = {
                diagnosis: this.state.diagnosis,
                pastIllnesses: this.state.pastIllnesses,
                presentDisease: this.state.presentDisease,
                date: new Date(),
                doctorId: this.props.doctor._id
            }
            this.props.createMedicalCard(this.props.match.params.id, data)
            this.setState({isAllowed: true, error: false})
        } else {
            this.setState({error: true})
        }
    };




    render() {

        const {medicalCards, questionnaire} = this.props

        return (
            <Row>
                <Col xs="12" md="12">
                    <h1 className={'medical-card-header-title'}>Медицинская карта стоматологического больного</h1>
                    <div>
                        <h2 style={{textAlign: 'center'}}>
                            {`${medicalCards && medicalCards.surname}
                            ${medicalCards && medicalCards.name}
                            ${medicalCards && medicalCards.thirdname}`}
                        </h2>
                    </div>
                </Col>
                {!questionnaire.length &&
                <Col sm="12" md="12" className={'medicalCard_button'}>
                    <Button
                        className='float-lg-right'
                        size={'sm'}
                        outline
                        color="primary"
                        disabled={!this.state.isAllowed}
                    >
                        <NavLink tag={RouterNavLink}
                                 className={'medicalCard_navLink'}
                                 to={`/patient_list/${this.props.match.params.id}/questionnaire`}
                                 exact
                                 disabled={!this.state.isAllowed}
                        >
                            Перейти к анкете
                        </NavLink>
                    </Button>
                </Col>
                }
                <Col xs="12" md="12">
                    <Label for="exampleText" sm={2} md={6}>
                        Диагноз
                    </Label>
                    <Input
                        type="textarea"
                        name="diagnosis"
                        id="exampleText"
                        onChange={this.inputChangeHandler}
                        value={this.state.diagnosis}
                        className={this.state.error ? 'diagnosis_field' : ''}
                    />
                    {
                        this.state.error && (
                            <Label for="exampleText" className={'label_error'} sm={2} md={6}>
                                Обязательное поле
                            </Label>
                        )
                    }
                </Col>
                <Col xs="12" md="12">
                    <Label for="exampleText" sm={2} md={6}>
                        Перенесенные и сопутствующие заболевания
                    </Label>
                    <Input
                        type="textarea"
                        name="pastIllnesses"
                        id="exampleText"
                        value={this.state.pastIllnesses}
                        onChange={this.inputChangeHandler}
                    />
                </Col>
                <Col xs="12" md="12">
                    <Label for="exampleText" sm={2} md={6}>
                        Развитие настоящего заболевания
                    </Label>
                    <Input
                        type="textarea"
                        name="presentDisease"
                        id="exampleText"
                        value={this.state.presentDisease}
                        onChange={this.inputChangeHandler}
                    />
                </Col>
                <Col sm="12" md={{size: 5, offset: 5}}>
                    <Button
                        className={'medicalCard_button'}
                        color="success"
                        onClick={this.saveInformation}
                    >
                        Сохранить
                    </Button>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = (state) => ({
    medicalCards: state.medicalCards.patient,
    questionnaire: state.medicalCards.questionnaire,
    doctor: state.users.doctorProfile
})

const mapDispatchToProps = (dispatch) => ({
    fetchPatientId: data => dispatch(fetchPatientId(data)),
    createMedicalCard: (id, data) => dispatch(createMedicalCard(id, data)),
    fetchQuestionnaire: id => dispatch(fetchQuestionnaire(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(MedicalCard);