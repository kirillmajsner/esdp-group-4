import React from 'react';
import {Card, CardBody, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";

const ClinicProfileItem = props => {
    return (
        <Card key={props._id} style={{marginTop: '10px'}}>


            <CardBody>
                <Link to={'/infoClinic/' + props._id}>
                    <CardTitle>
                        Название: {props.title}
                    </CardTitle>
                </Link>
                <p>Описание: {props.description}</p>
                <p>Адрес клиники: {props.address}</p>
                <p>Номер телефона: {props.phone}</p>
            </CardBody>
        </Card>
    );
};

export default ClinicProfileItem;