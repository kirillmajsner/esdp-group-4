import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import UserCabinetMenu from "../UserCabinetMenu/UserCabinetMenu";


class ClinicProfilePage extends Component {

    render() {
        const clinic = this.props.clinicProfile;
        return (
            <Row>
                <Col xs="12" md="3">
                    <UserCabinetMenu/>
                </Col>
                <Col xs="12" md="9">
                    <Fragment>
                        {this.props.clinicProfile ?
                            <div>
                                <h1>{clinic.title}</h1>

                                <p>Описание: {clinic.description}</p>
                                <p>Адрес: {clinic.address}</p>
                                <p>Номер телефона: {clinic.phone}</p>

                                <Link to='/cabinet/profile'>
                                    <Button id="editClinicProfile" color="primary">
                                        Редактировать данные
                                    </Button>
                                </Link>
                            </div>
                            : <p>Заполните анкету</p>}
                    </Fragment>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    error: state.profiles.error,
    clinicProfile: state.users.clinicProfile
});


export default connect(mapStateToProps)(ClinicProfilePage);